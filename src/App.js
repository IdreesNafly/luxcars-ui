import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Homepage from "./Pages/Homepage";
import "bootstrap/dist/css/bootstrap.min.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import CarListing from "./Pages/CarListing";
import Login from "./Pages/Login";
import Navbar from "./Components/Navbar";
import SignUp from "./Pages/Signup";
import "antd/dist/antd.css";
import "./index.css";
import DetailedCar from "./Pages/DetailedCar";
import adminhome from "./Admin/AdminDashboard";
import ManageUsers from "./Admin/ManageUsers";
import ManageVehicles from "./Admin/ManageVehicles";
import AddVehicles from "./Admin/AddVehicles";
import ManageAppointments from "./Admin/ManageAppointments";
import ManageAds from "./Admin/ManageAds";
import Checkout from "./Pages/Checkout";
import Contactus from "./Pages/Contactus";
import Aboutus from "./Pages/Aboutus";
import SellCar from "./Pages/SellCar";
import UserDashboard from "./Pages/UserDashboard";
import ImgTest from "./Admin/ImgTest";
import { BackTop } from "antd";
import { Container, Button, Link } from "react-floating-action-button";
import Chatbot from "./Components/Chatbot";
import ReservedVehicles from "./Admin/ReservedVehicles";
function App() {
  return (
    <Router>
      <div style={{ display: "flex", flex: 1 }}>
        <Navbar />
      </div>

      <Switch>
        <div
          style={{
            marginTop: "4rem",
            display: "flex",
            flex: 1
            // height: "100vh"
            // padding: "0rem 2rem 0rem 2rem"
          }}
        >
          <Route path="/" exact={true} component={Homepage} />
          <Route path="/login" exact={true} component={Login} />
          <Route path="/Signup" exact={true} component={SignUp} />
          <Route path="/carlisting" exact={true} component={CarListing} />
          <Route path="/carlisting/detailedcar/:id" component={DetailedCar} />
          <Route path="/checkout" component={Checkout} />
          <Route path="/contactus" component={Contactus} />
          <Route path="/sell-car" component={SellCar} />
          <Route path="/About" component={Aboutus} />
          <Route path="/Contact" component={Contactus} />
          <Route path="/userdashboard" component={UserDashboard} />
        </div>
      </Switch>

      <Route path="/adminhome" component={adminhome} />
      <Route path="/adminmanageusers" component={ManageUsers} />
      <Route path="/adminmanagevehicles" component={ManageVehicles} />
      <Route path="/adminmanageappointments" component={ManageAppointments} />
      <Route path="/adminmanageads" component={ManageAds} />
      <Route path="/adminaddvehicles" component={AddVehicles} />
      <Route path="/test" component={ImgTest} />
      <Route path="/adminreservedvehicles" component={ReservedVehicles} />
      <Route path="/:id/chatbot" component={Chatbot} />
      {/* <Chatbot /> */}
      <BackTop />
    </Router>
  );
}

export default App;
