import axios from "axios";
import { message } from "antd";
import cogoToast from "cogo-toast";

export const BASE_URL = "http://localhost:8080/";

// let config = {
//   headers: {
//     Authorization: type+" "token
//   }
// };

//admin
export const AddVehicle = val => {
  axios
    .post(BASE_URL + "api/vehicle/addVehicle", val)
    .then(function(res) {
      let data = res.data;
      if (res.status === 200) {
        cogoToast.success("Vehicle Added Successfully");
      }
    })
    .catch(err => {
      cogoToast.error("Error");
    });
};

export const ViewAllVehicles = () => {
  axios
    .get(BASE_URL + "api/vehicle/getAllVehicles")
    .then(res => {
      this.setState({
        vehicleData: res.data
      });
    })
    .catch(err => {});
};

export const getAllMake = () => {
  axios
    .get(BASE_URL + "api/vehicle/getAllMake")
    .then(res => {
      this.setState({
        vehicleMake: res.data
      });
    })
    .catch(err => {});
};

export const getAllModel = () => {
  axios
    .get(BASE_URL + "api/vehicle/getAllModel")
    .then(res => {
      this.setState({
        vehicleModel: res.data
      });
    })
    .catch(err => {});
};

export const getAllYear = () => {
  axios
    .get(BASE_URL + "api/vehicle/getAllYear")
    .then(res => {
      this.setState({
        vehicleYear: res.data
      });
    })
    .catch(err => {});
};

export const ViewAllAdVehicles = () => {};

export const DeleteVehicle = id => {
  axios
    .delete(BASE_URL + `/deletevehicle/${id}`)
    .then(res => {
      let data = res.data;
    })
    .catch(err => {});
};

export const UpdateVehiclePrice = value => {
  axios
    .put(BASE_URL + "/updatevehicle", value)

    .then(res => {
      let data = res.data;
    })
    .catch(err => {});
};

export const UpdateVehicleStatus = value => {
  const payload = {
    vehicleId: value,
    status: "Not_Available"
  };
  axios
    .put(BASE_URL + "api/vehicle/updateVehicleStatus", payload)

    .then(res => {
      let data = res.data;
    })
    .catch(err => {});
};
export const ViewAllUsers = () => {};
export const BlockUsers = () => {};
export const ViewAllAppointments = () => {};
export const RespondToAppointments = () => {};
export const RespondToAds = () => {};

//user:
export const ViewVehicleById = () => {};
export const ViewVehiclesByUser = () => {};
export const ViewSavedVehicles = () => {};
export const AddToSavedVehicles = () => {};

export const MakeAppointments = (payload, token, type) => {
  axios
    .post(BASE_URL + "api/appointments/makeAppointments", payload, {
      headers: {
        Authorization: type + " " + token
      }
    })
    .then(function(res) {
      console.log(res.data);
      cogoToast.success("Your appointment is made");
    })
    .catch(err => {
      cogoToast.error("Error");
    });
};
export const ViewAppointmentsByUser = () => {};
export const PostAd = () => {};
export const SearchVehicle = () => {};
export const ViewUserById = () => {};
export const ShareVehicle = () => {};

export const MakeVehicleReview = () => {};
export const ViewReviewByVehicle = () => {};
export const EditReview = () => {};
export const DeleteReview = () => {};
