import React, { Component } from "react";
import styled from "styled-components";
import { Divider, Input, Upload, Card } from "antd";
import {
  MDBJumbotron,
  MDBBtn,
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCardTitle,
  MDBIcon
} from "mdbreact";
import { Select, Form, InputNumber, Radio, Button } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import luxcars from "../Images/luxcars.jpg";
import { AddVehicle } from "../services";
import axios from "axios";
import { BASE_URL } from "../services";
const { Option } = Select;
const { Meta } = Card;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  margin: 0 5rem 0 5rem;
`;
const Header = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  align-items: center;
  color: darkblue;
`;
const Title = styled.span`
  display: flex;
  font-size: 3rem;
  font-weight: bold;
`;

const Poster = styled.div`
  display: flex;
  flex: 1;
`;

const DetailWrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  background: #0559ad;
  height: 15vh;
  align-items: center;
`;
const DropdownWrap = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  margin: 0 3rem 0 3rem;
`;
const SubTitle = styled.div`
  font-size: 1.5rem;
  color: darkblue;
  font-weight: 400;
  font-variant: petite-caps;
`;

const fileList = [
  {
    uid: "-1",
    name: "xxx.png",
    status: "done",
    url:
      "https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png",
    thumbUrl:
      "https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
  },
  {
    uid: "-2",
    name: "yyy.png",
    status: "error"
  }
];

const props = {
  action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
  listType: "picture",
  defaultFileList: [...fileList]
};

class SellCar extends Component {
  state = {};
  componentDidMount() {
    const type = localStorage.getItem("type");
    const token = localStorage.getItem("token");
    const email = localStorage.getItem("email");

    this.setState({
      type: type,
      token: token,
      email: email
    });
  }

  render() {
    return (
      <Wrapper>
        <Header>
          <Title>Selling or trading in?</Title>
          {/* <SubTitle>
            At LuxCars, we'll buy your car even if you don't buy ours®. Stop by
            anytime or make an appointment below.
          </SubTitle> */}
        </Header>
        <Poster>
          <Card
            hoverable
            style={{
              display: "flex",
              flex: 1,
              height: "30vh",
              margin: "1rem 0 1rem 0",
              //background: "#ffd900"
              background: "linear-gradient(40deg, #4a4046, #2b2796)"
            }}
          >
            <section className="text-center ">
              <MDBRow>
                <MDBCol md="4">
                  <MDBIcon icon="car-side" size="3x" className="red-text" />
                  <h5 className="font-weight-bold my-4">
                    Tell us about your car
                  </h5>
                  <p className="white-text mb-md-0 mb-5">
                    Ready to sell? Stop by anytime we're open or schedule your
                    fast, free appraisal in advance.
                  </p>
                </MDBCol>
                <MDBCol md="4">
                  <MDBIcon icon="dollar-sign" size="3x" className="cyan-text" />
                  <h5 className="font-weight-bold my-4">Get your Offer</h5>
                  <p className="white-text mb-md-0 mb-5">
                    Our offer is good for 7 days and stays the same whether you
                    trade in or simply sell.
                  </p>
                </MDBCol>
                <MDBCol md="4">
                  <MDBIcon
                    far
                    icon="comments"
                    size="3x"
                    className="orange-text"
                  />
                  <h5 className="font-weight-bold my-4">Make your Decision</h5>
                  <p className="white-text mb-md-0 mb-5">
                    You can take payment, apply to trade-in, or give yourself a
                    week to think about it.
                  </p>
                </MDBCol>
              </MDBRow>
            </section>
          </Card>
        </Poster>
        <Title style={{ fontSize: "2rem" ,color:"black",fontVariantCaps:"small-caps",justifyContent:"center"}}>Build Your Ad</Title>
        <Form name="validate_other" onFinish={this.onFinish}>
          <SubTitle>Add Vehicle </SubTitle>
          <Divider />
          <DetailWrapper>
            {/* <DropdownWrap>
              <div style={{ color: "white", fontWeight: "bold" }}>
                Select Condition*
              </div>

              <Input />
            </DropdownWrap> */}
            <DropdownWrap>
              <div style={{ color: "white", fontWeight: "bold" }}>
                Enter Make*
              </div>
              <Form.Item
                name="vehicleMake"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: "Please enter make!"
                  }
                ]}
              >
                <Input />
              </Form.Item>
            </DropdownWrap>
            <DropdownWrap>
              <div style={{ color: "white", fontWeight: "bold" }}>
                Enter Model*
              </div>
              <Form.Item
                name="vehicleModel"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: "Please enter model!"
                  }
                ]}
              >
                <Input />
              </Form.Item>{" "}
            </DropdownWrap>
            <DropdownWrap>
              <div style={{ color: "white", fontWeight: "bold" }}>
                Enter Year*
              </div>
              <Form.Item
                name="vehicleYom"
                hasFeedback
                rules={[
                  { required: true, message: "Please select vehicle Yom!" }
                ]}
              >
                <InputNumber style={{width:"100%"}} min={1990} max={2020} />
              </Form.Item>
            </DropdownWrap>
          </DetailWrapper>
          <Divider />
          <div style={{ margin: "1rem" , display:"flex",flex:1,flexDirection:"row" }}>
            <div style={{display:"flex",flex:1,flexDirection:"column"}}>

              
            <Form.Item
              name="vehicleType"
              label="Vehicle Type"
              hasFeedback
              rules={[
                { required: true, message: "Please select Vehicle Type!" }
              ]}
            >
              <Select placeholder="Please select Vehicle Type">
                <Option value="suv">SUV</Option>
                <Option value="sedan">SEDAN</Option>
                <Option value="coupe">COUPE</Option>
                <Option value="van">VAN</Option>
              </Select>
            </Form.Item>

            <Form.Item
              name="vehicleExteriorColor"
              label="Vehicle Exterior Color"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "Please select Vehicle Exterior Color!"
                }
              ]}
            >
              <Input placeholder="" />
            </Form.Item>

            <Form.Item
              name="vehicleInteriorColor"
              label="Vehicle Interior Color"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "Please select Vehicle Interior Color!"
                }
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="vehicleDescription"
              label="Vehicle Description;"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "Please enter Vehicle Description!"
                }
              ]}
            >
              <Input placeholder="Please enter Vehicle Description" />
            </Form.Item>

            </div>
            <Divider type="vertical" style={{height:"15rem%", marginRight:"2rem",marginLeft:"2rem"}}/>
            <div style={{display:"flex",flex:1,flexDirection:"column"}}>
           
            <Form.Item
              name="vehicleFuel"
              label="Vehicle Fuel"
              hasFeedback
              rules={[
                { required: true, message: "Please select Vehicle Fuel!" }
              ]}
            >
              <Radio.Group>
                <Radio.Button value="Hybrid">Hybrid</Radio.Button>
                <Radio.Button value="Petrol">Petrol</Radio.Button>
                <Radio.Button value="Diesel">Diesel</Radio.Button>
              </Radio.Group>
            </Form.Item>

            <Form.Item
              name="vehicleTransmission"
              label="Vehicle Transmission"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "Please select vehicleTransmission!"
                }
              ]}
            >
              <Radio.Group>
                <Radio.Button value="Automatic">Automatic</Radio.Button>
                <Radio.Button value="Manual">Manual</Radio.Button>
              </Radio.Group>
            </Form.Item>

            <Form.Item
              name="vehicleMileage"
              label="Vehicle Mileage"
              hasFeedback
              rules={[
                { required: true, message: "Please enter Vehicle Mileage!" }
              ]}
            >
              <InputNumber style={{width:"38%"}} min={1} max={100000} />
            </Form.Item>
            </div>

          </div>
          <SubTitle>Upload Images</SubTitle>
          <Divider />
          <input
            type="file"
            name="file"
            multiple
            onChange={this.onChangeHandler}
          />

          {/* </Form.Item> */}
          <SubTitle style={{marginTop:"2rem"}}>Enter Expected Price</SubTitle>
          <Divider />
          <Card
            hoverable
            style={{
              display: "flex",
              flex: 1,
              height: "11vh",
              //background: "#0559ad",
              justifyContent: "center",
              flexDirection: "row",
              marginBottom: "2rem",
              background:"#e8e8e8"
            }}
          >
            {/* <Form.Item name="vehiclePrice">
              <div
                style={{ display: "flex", color: "white", fontWeight: "bold" }}
              >
                Enter Price:*
              </div>
              <InputNumber style={{ width: "50vw" }} size="large" />
            </Form.Item> */}
            <Form.Item
              name="vehiclePrice"
              size="large"
              hasFeedback
              rules={[
                { required: true, message: "Please enter Vehicle Price!" }
              ]}
            >
             LKR:  <InputNumber style={{width:"45rem"}} min={1} max={100000000} />
            </Form.Item>
            <div style={{ display: "flex", flex: 3 }}></div>
          </Card>
          <Form.Item>
            <div style={{ display: "flex", flex: 1, justifyContent: "center" }}>
              <Button
                style={{ width: "40vw",height:"3rem",fontVariantCaps:"all-petite-caps",fontSize:"1.5rem" }}
                type="primary"
                htmlType="submit"
              >
                Post Vehicle Ad
              </Button>
            </div>
          </Form.Item>
        </Form>
      </Wrapper>
    );
  }

  onFinish = values => {
    const email = this.state.email;
    const token = this.state.token;
    const type = this.state.type;
    values.userid = email;

    let images = this.state.selectedFile;
    const data = new FormData();
    const img = Object.values(images);
    console.log(values);
    console.log(images);
    console.log(img);
    img.map((val, index) => data.append("files", val));
    data.append("vehicle", JSON.stringify(values));
    this.AddPostVehicle(data, type, token);
  };

  onChangeHandler = event => {
    console.log(event.target.files[0]);
    this.setState({
      selectedFile: event.target.files[0],
      loaded: 0
    });
  };

  AddPostVehicle = (val, type, token) => {
    console.log(val);
    axios
      .post(BASE_URL + "api/vehicle/postAd", val, {
        headers: {
          Authorization: type + " " + token
        }
      })
      .then(function(res) {
        let data = res.data;
        if (res.status === 200) {
          alert("Your post has been sent for Approval!");
        }
      })
      .catch(err => {});
  };

  onChangeHandler = event => {
    console.log(event.target.files);
    this.setState({
      selectedFile: event.target.files,
      loaded: 0
    });
  };
}

function onChange(value) {
  console.log(`selected ${value.file}`);
}

function onBlur() {
  console.log("blur");
}

function onFocus() {
  console.log("focus");
}

function onSearch(val) {
  console.log("search:", val);
}

export default SellCar;
