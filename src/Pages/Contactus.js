import React, { Component } from "react";
import {
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBIcon,
  MDBBtn,
  MDBInput
} from "mdbreact";
import Axios from "axios";
import cogoToast from "cogo-toast";
import { BASE_URL } from "../services";

class Contactus extends Component {
  state = {
    email: "",
    subject: "",
    body: ""
  };
  handleBodyChange = e => {
    this.setState({
      body: e.target.value
    });
  };
  handleEmailChange = e => {
    this.setState({
      email: e.target.value
    });
  };
  handleSubjectChange = e => {
    this.setState({
      subject: e.target.value
    });
  };
  render() {
    return (
      <div style={{ display: "flex", flex: 1, justifyContent: "center" }}>
        <section style={{ marginTop: "0rem" }}>
          <h2 className="h1-responsive font-weight-bold text-center">
            Contact us
          </h2>
          <MDBRow>
            <MDBCol lg="5" className="lg-0 mb-4">
              <MDBCard>
                <MDBCardBody>
                  <div
                    style={{ padding: "1rem" }}
                    className="form-header amber"
                  >
                    <h3 className="mt-2">
                      <MDBIcon icon="envelope" /> Write to us:
                    </h3>
                  </div>
                  <p className="dark-grey-text">
                    Feel Free to write us about anything you want to ask us.
                    LuxCars Will be more than happy to help!
                  </p>
                  <div className="md-form">
                    <MDBInput
                      icon="user"
                      label="Your name"
                      iconClass="grey-text"
                      type="text"
                      id="form-name"
                    />
                  </div>
                  <div className="md-form">
                    <MDBInput
                      icon="envelope"
                      label="Your email"
                      iconClass="grey-text"
                      type="text"
                      id="form-email"
                      onChange={e => {
                        this.handleEmailChange(e);
                      }}
                      value={this.state.email}
                    />
                  </div>
                  <div className="md-form">
                    <MDBInput
                      icon="tag"
                      label="Subject"
                      iconClass="grey-text"
                      type="text"
                      id="form-subject"
                      onChange={e => {
                        this.handleSubjectChange(e);
                      }}
                      value={this.state.subject}
                    />
                  </div>
                  <div className="md-form">
                    <MDBInput
                      icon="pencil-alt"
                      label="Your Message"
                      iconClass="grey-text"
                      type="textarea"
                      id="form-text"
                      onChange={e => {
                        this.handleBodyChange(e);
                      }}
                      value={this.state.body}
                    />
                  </div>
                  <div className="text-center">
                    <MDBBtn
                      color="blue darken-4"
                      onClick={e => {
                        this.sendEmail();
                      }}
                    >
                      Submit
                    </MDBBtn>
                  </div>
                </MDBCardBody>
              </MDBCard>
            </MDBCol>
            <MDBCol lg="7">
              <div
                id="map-container"
                className="rounded z-depth-1-half map-container"
                style={{ height: "400px" }}
              >
                <iframe
                  src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d76765.98321148289!2d-73.96694563267306!3d40.751663750099084!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spl!2spl!4v1525939514494"
                  title="This is a unique title"
                  width="100%"
                  height="100%"
                  frameBorder="0"
                  style={{ border: 0 }}
                />
              </div>
              <br />
              <MDBRow className="text-center">
                <MDBCol md="4">
                  <MDBBtn
                    tag="a"
                    floating
                    color="blue darken-4"
                    className="accent-1"
                  >
                    <MDBIcon icon="map-marker-alt" />
                  </MDBBtn>
                  <p>Colombo</p>
                  <p className="mb-md-0">Sri Lanka</p>
                </MDBCol>
                <MDBCol md="4">
                  <MDBBtn
                    tag="a"
                    floating
                    color="blue darken-4"
                    className="accent-1"
                  >
                    <MDBIcon icon="phone" />
                  </MDBBtn>
                  <p>+ 01 234 567 89</p>
                  <p className="mb-md-0">Mon - Fri, 8:00-22:00</p>
                </MDBCol>
                <MDBCol md="4">
                  <MDBBtn
                    tag="a"
                    floating
                    color="blue darken-4"
                    className="accent-1"
                  >
                    <MDBIcon icon="envelope" />
                  </MDBBtn>
                  <p>info@luxcars.com</p>
                  <p className="mb-md-0">sale@luxcars.com</p>
                </MDBCol>
              </MDBRow>
            </MDBCol>
          </MDBRow>
        </section>
      </div>
    );
  }

  sendEmail = () => {
    const email = this.state.email;
    const subject = this.state.subject;
    const body = this.state.body;

    const payload = {
      email: email,
      subject: subject,
      body: body
    };

    Axios.post(BASE_URL + "mailservice/mailto", payload)
      .then(res => {
        if (res.status == 200) {
          cogoToast.info(
            "Your email has been sent.We will get in touch with you soon!"
          );
        }
      })
      .catch(err => cogoToast.error("Error"));
  };
}

export default Contactus;
