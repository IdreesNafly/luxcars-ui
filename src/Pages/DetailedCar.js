import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import {
  DatePicker,
  Input,
  Divider,
  Modal,
  Card,
  Statistic,
  Row,
  Col,
  Button,
  Form,
  message,
  notification,
  Descriptions,
  Popconfirm
} from "antd";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import ranger from "../Images/ranger.jpg";
import def from "../Images/defendere.jpg";
import "../Stylesheets/AdminStyles.css";
import "../Stylesheets/descriptions.css";
import { Link, Redirect } from "react-router-dom";
import { HeartFilled } from "@ant-design/icons";
import { MakeAppointments } from "../services";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { MDBNotification } from "mdbreact";
import cogoToast from "cogo-toast";

// import {UpdateVehicleStatus} from '../services';
import axios from "axios";
import Review from "../Components/Reviews";

const BASE_URL = "http://localhost:8080/";

const { confirm } = Modal;
const { Meta } = Card;
const { TextArea } = Input;

const Div = styled.div`
  padding: 3rem;
  display: flex;
  flx: 1;
  flex-direction: column;
`;
const Wrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
`;

const RightWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 3;
`;
const LeftWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

const Header = styled.div`
  display: flex;
  flex: 1;
`;

const ImageWrapper = styled.div`
  display: flex;
  flex: 1;
`;

const Body = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const Title = styled.span`
  display: flex;
  font-size: 2rem;
  font-weight: bold;
`;

const ItemDetail = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  padding: 0.2rem;
`;

const Item = styled.div`
  display: flex;
  flex: 1;
  font-size: 1.2rem;
  font-family: initial;
  color: black;
`;

const valuestyle = {
  fontSize: "1.5rem",
  color: "blue",
  fontWeight: "bold",
  fontStyle: "italic"
};

function showConfirm() {
  confirm({
    title: "Are you sure you want to reserve this vehicle?",
    icon: <ExclamationCircleOutlined />,
    content: "You will recieve a confirmation if you confirm!",
    okText: "Confirm",
    okType: "confirm",
    cancelText: "Go Back",
    onOk() {},
    onCancel() {
      console.log("Cancel");
    }
  });
}

class DetailedCar extends Component {
  state = { visible: false, isReserved: false };

  onChangeDate(value, dateString) {
    console.log("Selected Time: ", value);
    console.log("Formatted Selected Time: ", dateString);
  }

  onOk(value) {
    console.log("onOk: ", value);
  }

  showModal = () => {
    this.setState({
      visible: true
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  componentDidMount() {
    console.log(this.props.location.state.vehicleData);
    const token = localStorage.getItem("token");
    const type = localStorage.getItem("type");
    const mail = localStorage.getItem("email");

    this.setState({
      token: token,
      type: type,
      email: mail
    });
  }

  render() {
    let data = null;
    if (this.props.location.state.vehicleData) {
      data = this.props.location.state.vehicleData;
    }

    const filteredData = {
      ReferenceID: data.id,
      Make: data.vehicleMake,
      Model: data.vehicleModel,
      Type: data.vehicleType,
      YOM: data.vehicleYom,
      Fuel: data.vehicleFuel,
      Transmission: data.vehicleTransmission,
      Mileage: data.vehicleMileage,
      Exterior_Color: data.vehicleExteriorColor,
      Interior_Color: data.vehicleInteriorColor
    };
    return (
      <Div>
        <Header>
          <Title>
            {data.vehicleMake + " " + data.vehicleModel + " " + data.vehicleYom}
          </Title>
        </Header>
        <Divider />
        {/* <CustDivider /> */}
        <Wrapper>
          <RightWrapper>
            <ImageWrapper>{this.renderImages(data.imageList)}</ImageWrapper>
            {/* <Divider /> */}
            <Body>
              <Divider orientation="left">Car Details</Divider>
              <div>
                {Object.entries(filteredData).map(([key, value]) => {
                  return (
                    // <ItemDetail>
                    <Descriptions title="" bordered>
                      <Descriptions.Item label={key.toLocaleUpperCase()}>
                        {value}
                      </Descriptions.Item>
                    </Descriptions>
                    /* <Item>{key.toLocaleUpperCase()}</Item>
                      <Divider type="vertical" />
                      <Item>{value}</Item> */
                    // </ItemDetail>
                  );
                })}
              </div>
              <Divider orientation="left">Features and Specs</Divider>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed
                nonne merninisti licere mihi ista probare, quae sunt a te dicta?
                Refert tamen, quo modo.
              </p>
            </Body>
          </RightWrapper>
          <Divider type="vertical"></Divider>
          <LeftWrapper>
            <Card hoverable>
              <div
                style={{
                  display: "flex",
                  flex: 1,
                  justifyContent: "space-between"
                }}
              >
                <Meta title={data.vehicleModel + " - " + data.vehicleYom} />
                {/* <button>Add To Saved</button> */}
                <HeartFilled
                  onClick={e => this.addtoSavedList(data.id)}
                  style={{
                    fontSize: "2rem",
                    color: "hotpink"
                  }}
                />
              </div>
              <Divider />
              <div>{data.vehicleDescription}</div>
              <Divider />
              <Row gutter={16}>
                <Col span={12}>
                  <Statistic
                    title="Mileage (Miles)"
                    value={data.vehicleMileage}
                    valueStyle={valuestyle}
                  />
                </Col>
                <Col span={12}>
                  <Statistic
                    title="Vehicle Price in LKR"
                    value={data.vehiclePrice}
                    precision={2}
                    valueStyle={valuestyle}
                  />
                </Col>
              </Row>
              <br />
              <div>Stock# 18003075 </div>
              <div>VIN # 5XYKT3A19CG290875</div>
              <Divider />
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  flex: 1,
                  padding: "1rem"
                }}
              >
                <div style={{ fontWeight: "bold" }}>
                  In Stock at LuxCars International
                </div>
                <Link
                  style={{
                    fontVariantNumeric: "oldstyle-nums",
                    marginBottom: "2rem"
                  }}
                >
                  {"(+9477235850)"}
                </Link>
                <Button
                  style={{
                    display: "flex",
                    height: "3rem",
                    fontWeight: "500",
                    background: "#efd13f",
                    justifyContent: "center"
                  }}
                  onClick={e =>
                    this.state.email === null
                      ? (window.location.href = "/login")
                      : this.showModal()
                  }
                >
                  Request for Inspection
                </Button>
                <div>No Obligation - No Pressure - No Deposit</div>
              </div>
              <div style={{ background: "#efd13f", padding: "1rem" }}>
                <div style={{ fontSize: "1rem", fontWeight: 500 }}>
                  This Vehicle is LuxCars Certified
                </div>

                <div style={{ color: "black", fontStyle: "italic" }}>
                  Every car we sell undergoes an extensive reconditioning
                  process to meet our high standards.
                </div>
              </div>
              <div style={{ marginTop: "2rem" }}>
                {/* <Link
                  to={{
                    pathname: `/checkout`,
                    state: { vehicleData: data }
                  }}
                > */}
                {this.state.isReserved ? (
                  <Redirect
                    to={{
                      pathname: `/checkout`,
                      state: { vehicleData: data }
                    }}
                  />
                ) : (
                  ""
                )}
                <Popconfirm
                  title={"Are you sure you want to reserve this vehicle?"}
                  onConfirm={e => {
                    this.UpdateVehicleStatus(
                      data.id,
                      this.state.token,
                      this.state.type,
                      this.state.email
                    );
                  }}
                  okText="Yes"
                  cancelText="No"
                >
                  <Button
                    style={{
                      display: "flex",
                      height: "3rem",
                      fontWeight: "bold",
                      background: "#000",
                      justifyContent: "center",
                      fontVariant: "all-small-caps",
                      width: "100%",
                      fontSize: "1.1rem",
                      color: "white"
                    }}
                  >
                    Reserve this Vehicle*
                  </Button>
                </Popconfirm>
                {/* </Link> */}
                <div>{"Terms & Conditions Apply"}</div>
              </div>
            </Card>
          </LeftWrapper>
        </Wrapper>
        <Modal
          title="Make An Appointment for Inspection"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          maskClosable
          mask
          cancelButtonProps={{ disabled: true }}
        >
          <Form
            labelCol={{ span: 4 }}
            wrapperCol={{ span: 14 }}
            layout="horizontal"
            onFinish={this.onFinish}
          >
            <Form.Item name="appointmentDateTime" label={"Date & Time: "}>
              <DatePicker showTime />
            </Form.Item>
            <Form.Item name="message" label="Message: ">
              <TextArea
                placeholder="Autosize height with minimum and maximum number of lines"
                autoSize={{ minRows: 2, maxRows: 6 }}
              />
            </Form.Item>
            <Form.Item>
              <Button htmlType="submit">Make Appointment</Button>
            </Form.Item>
          </Form>
        </Modal>
      </Div>
    );
  }
  onFinish = values => {
    const payload = {
      message: values.message,
      appointmentDateTime: values.appointmentDateTime._d,
      vehicleid: this.props.location.state.vehicleData.id,
      useremail: this.state.email
    };

    MakeAppointments(payload, this.state.token, this.state.type);
  };
  renderImages = data => {
    return (
      <Carousel
        showArrows={true}
        onChange={this.onChange}
        onClickItem={this.onClickItem}
        onClickThumb={this.onClickThumb}
        useKeyboardArrows
      >
        {data && data.length > 0 ? (
          data.map((image, index) => {
            return (
              <div>
                <img src={image} />
                <p className="legend">{"Legend " + index}</p>
              </div>
            );
          })
        ) : (
          <div>
            <img src={ranger} />
            <p className="legend">Legend 1</p>
          </div>
        )}
      </Carousel>
    );
  };

  onChange = () => {
    console.log("onchange");
  };

  onClickItem = () => {
    console.log("onclickItem");
  };
  onClickThumb = () => {
    console.log("onclickThumb");
  };

  addtoSavedList(vehicleid) {
    const id = this.state.email;
    const payload = {
      userid: id,
      vehicleid: vehicleid
    };
    axios
      .post(BASE_URL + "api/savedlist/addtosavedlist", payload, {
        headers: {
          Authorization: this.state.type + " " + this.state.token
        }
      })
      .then(res => {
        if (res.status === 200) {
          cogoToast.success("Vehicle Added to Saved List");
          //message.success("Added to Saved List");
          // notification.open({
          //   message: "Saved List",
          //   description:
          //     "Vehicle with ID " +
          //     vehicleid +
          //     " has been added to your saved list",
          //   onClick: () => {
          //     console.log("Notification Clicked!");
          //   }
          // });
        }
      })
      .catch(err => {
        console.log(err.response);
        cogoToast.error(err.response.data.message||err.response.data);
      });
  }

  UpdateVehicleStatus = (value, token, type, email) => {
    if (token === null) {
      window.location.href = "/login";
    }
    console.log(token);
    const payload = {
      vehicleId: value,
      status: "Reserved",
      reservedBy: email
    };
    axios
      .put(BASE_URL + "api/vehicle/updateVehicleStatus", payload, {
        headers: {
          Authorization: type + " " + token
        }
      })
      .then(res => {
        if (res.status === 200) {
          cogoToast.success("Vehicle Reserved");
          this.setState({
            isReserved: true
          });
        }
      })
      .catch(err => {
        cogoToast.error("Error");
        this.setState({
          isReserved: false
        });
      });
  };
}

export default withRouter(DetailedCar);
