import React, { Component } from "react";
import "../Stylesheets/PageStyles.css";
import {
  Form,
  Input,
  Select,
  Button,
  Alert,
  message,
  Card,
  Divider
} from "antd";
import { QuestionCircleOutlined } from "@ant-design/icons";
import axios from "axios";
import { ViewAllUsers } from "../services";
import luxcars from "../Images/luxcars.jpg";
import { Link } from "react-router-dom";
import { List, Avatar } from "antd";

const { Option } = Select;
const BASE_URL = "http://localhost:8080/";
// const [form] = Form.useForm();

const data = [
  {
    title: "Access saved cars and searches",
    img: "https://www.carmax.com/mycarmax/img/saved-searches.svg"
  },
  {
    title: "Reserve Vehicles with Zero Cost",
    img: "https://www.carmax.com/mycarmax/img/saved-cars.svg"
  },
  {
    title: "Make appointments to Inspect vehicles",
    img: "https://www.carmax.com/mycarmax/img/account.svg"
  },
  {
    title: "Set up new inventory alerts",
    img: "https://www.carmax.com/mycarmax/img/car-alerts.svg"
  }
];
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};

class Signup extends Component {
  state = {};

  onFinish = values => {
    console.log("Received values of form: ", values);

    let payload = {
      email: values.email,
      fullName: values.fullName,
      contactNo: values.contactNo,
      password: values.password
    };
    axios
      .post(BASE_URL + "api/auth/signup", payload)
      .then(res => {
        console.log(res);
        if (res.status === 200) {
          message.success("Registration Successful");
          setTimeout(this.props.history.push("/"), 3000);
        }
      })
      .catch(err => {
        console.log(err);
        message.error("Error");
      });
  };
  prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select
        style={{
          width: 70
        }}
      >
        <Option value="94">+94</Option>
      </Select>
    </Form.Item>
  );
  render() {
    return (
      <div className="loginWrapper">
        <Card
          style={{ width: "50%", height: "75%" }}
          bodyStyle={{ display: "flex", flexDirection: "row" }}
        >
          <div
            style={{
              display: "flex",
              flex: 1,
              marginRight: "1.5rem",
              flexDirection: "column"
            }}
          >
            <span
              style={{
                fontSize: "1.5rem",
                color: "darkblue",
                fontWeight: 400,
                fontVariantCaps: "petite-caps",
                marginBottom: "3rem"
              }}
            >
              Why create a LuxCars Account?
            </span>
            <List
              itemLayout="horizontal"
              dataSource={data}
              renderItem={item => (
                <List.Item>
                  <List.Item.Meta
                    avatar={<Avatar shape="square" src={item.img} />}
                    title={<a href="/">{item.title}</a>}
                  />
                </List.Item>
              )}
            />
          </div>
          <Divider
            type="vertical"
            style={{
              height: "25rem",
              background: "#e3dfdf",
              marginRight: "2rem"
            }}
          />
          <Form
            {...formItemLayout}
            // form={form}
            name="register"
            onFinish={this.onFinish}
            initialValues={{
              prefix: "94"
            }}
            scrollToFirstError
          >
            <Form.Item
              name="email"
              label="E-mail"
              rules={[
                {
                  type: "email",
                  message: "The input is not valid E-mail!"
                },
                {
                  required: true,
                  message: "Please input your E-mail!"
                }
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="password"
              label="Password"
              rules={[
                {
                  required: true,
                  message: "Please input your password!"
                }
              ]}
              hasFeedback
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              name="confirm"
              label="Confirm Password"
              dependencies={["password"]}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "Please confirm your password!"
                },
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (!value || getFieldValue("password") === value) {
                      return Promise.resolve();
                    }

                    return Promise.reject(
                      "The two passwords that you entered do not match!"
                    );
                  }
                })
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              name="fullName"
              label={<span>Full Name&nbsp;</span>}
              rules={[
                {
                  required: true,
                  message: "Please input your full name!",
                  whitespace: true
                }
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="contactNo"
              label="Phone Number"
              rules={[
                {
                  required: true,
                  message: "Please input your phone number!"
                }
              ]}
            >
              <Input
                addonBefore={this.prefixSelector}
                style={{
                  width: "100%"
                }}
              />
            </Form.Item>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
              >
                Sign up
              </Button>
            </Form.Item>
            <a href="/login">Already have an Account? Login here!</a>
          </Form>
        </Card>
      </div>
    );
  }
}

export default Signup;
