import React, { Component } from "react";
import Navbar from "../Components/Navbar";
import HomepageIntro from "../Components/HomepageIntro";
import FeaturedCars from "../Components/FeaturedCars";
import {
  MDBBtn,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBCardTitle,
  MDBCardText,
  MDBCol
} from "mdbreact";
import FooterPage from "../Components/Footer";

class Homepage extends Component {
  state = {};
  render() {
    return (
      <div style={{ display: "flex", flexDirection: "column", flex: 1 }}>
        {/* <Navbar /> */}
        <HomepageIntro />
        <div>{Card()}</div>

        {/* <FeaturedCars /> */}
        <FooterPage />
      </div>
    );
  }
}

const Card = () => {
  return (
    <MDBCol
      style={{
        display: "flex",
        flex: "1",
        flexDirection: "row",
        justifyContent: "space-evenly"
      }}
    >
      <MDBCard style={{ width: "22rem" }}>
        <MDBCardImage
          className="img-fluid"
          src="https://www.carmax.com/home/images/home/story/our-sticker.png"
          waves
        />
        <MDBCardBody>
          <MDBCardTitle>Buy Vehicle</MDBCardTitle>
          <MDBCardText>
            Some quick example text to build on the card title and make up the
            bulk of the card&apos;s content.
          </MDBCardText>
          <MDBBtn gradient="blue" href="/carlisting">
            Buy now
          </MDBBtn>
        </MDBCardBody>
      </MDBCard>
      <MDBCard style={{ width: "22rem" }}>
        <MDBCardImage
          className="img-fluid"
          src="https://www.carmax.com/home/images/home/story/no-haggle.png"
          waves
        />
        <MDBCardBody>
          <MDBCardTitle>Sell Your Vehicle</MDBCardTitle>
          <MDBCardText>
            Some quick example text to build on the card title and make up the
            bulk of the card&apos;s content.
          </MDBCardText>
          <MDBBtn gradient="blue" href="/sell-car">
            Sell Now
          </MDBBtn>
        </MDBCardBody>
      </MDBCard>
      <MDBCard style={{ width: "22rem" }}>
        <MDBCardImage
          className="img-fluid"
          src="https://www.carmax.com/home/images/home/story/change-your-mind.png"
          waves
        />
        <MDBCardBody>
          <MDBCardTitle>Enjoy Extra Benefits</MDBCardTitle>
          <MDBCardText>
            Some quick example text to build on the card title and make up the
            bulk of the card&apos;s content.
          </MDBCardText>
          <MDBBtn gradient="blue" href="/">
            More...
          </MDBBtn>
        </MDBCardBody>
      </MDBCard>
    </MDBCol>
  );
};

export default Homepage;
