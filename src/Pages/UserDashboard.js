import React, { Component } from "react";
import "../Stylesheets/UserDashboard.css";
import { Card, Avatar, Divider, Descriptions, Badge, Button } from "antd";
import { UserOutlined } from "@ant-design/icons";
import { Tabs, Skeleton } from "antd";
import { MakeAppointments } from "../services";
import axios from "axios";
import Spinner from "../Components/Spinner";
import TabsContent from "../Components/TabsContent";
import {
  EditOutlined,
  EllipsisOutlined,
  SettingOutlined
} from "@ant-design/icons";
const { Meta } = Card;

const { TabPane } = Tabs;
const BASE_URL = "http://localhost:8080/";

const style = {
  display: "flex",
  flex: 1,
  flexDirection: "row"
};

function callback(key) {
  console.log(key);
  // if (key === 1) {
  //   return this.Appointments();
  // } else if (key === 2) {
  //   return this.ReservedVehicles();
  // }
}
class UserDashboard extends Component {
  state = {
    appointmentLoading: true,
    vehicleLoading: true,
    adLoading: true,
    userLoading: true
  };
  componentDidMount() {
    const token = localStorage.getItem("token");
    const type = localStorage.getItem("type");
    const mail = localStorage.getItem("email");

    this.setState({
      token: token,
      type: type,
      email: mail
    });

    // this.getAppointmentData(type, token, mail);
    // this.getMyAds(type, token, mail);
    // this.getReservedVehicles(type, token, mail);
    this.getuser(type, token, mail);
    // this.getSavedList(type, token, mail);
  }
  render() {
    const userdata = this.state.userDetails;
    return (
      <div className="Wrapper">
        <div className="InnerWrapper">
          <Card
            hoverable
            style={{
              display: "flex",
              flex: 1,
              //background: "rgb(255, 217, 0)",
              background: "linear-gradient(40deg, rgb(37, 106, 165), rgb(81, 149, 232))"
              
              // border: " 2px solid darkblue"
            }}
            bodyStyle={style}
          >
            <Skeleton loading={this.state.userLoading}>
              <div style={{ display: "flex", flex: 1 }}>
                <Avatar
                  size={128}
                  src="https://www.pinclipart.com/picdir/middle/355-3553881_stockvader-predicted-adig-user-profile-icon-png-clipart.png"
                />
              </div>
              <Divider
                style={{ height: "100%", width: "3px", background: "darkblue" }}
                type="vertical"
              />
              <div style={{ display: "flex", flex: 9 }}>
                {userdata ? (
                  <Descriptions title="My Details">
                    <Descriptions.Item label="Full Name">
                      {userdata.fullName}
                    </Descriptions.Item>
                    <Descriptions.Item label="Email">
                      {userdata.email}
                    </Descriptions.Item>
                    <Descriptions.Item label="Telephone No">
                      {userdata.contactNo}
                    </Descriptions.Item>
                    <Descriptions.Item label="Remark">empty</Descriptions.Item>
                    <Descriptions.Item label="Address">
                      No. 18, Wantang Road, Xihu District, Hangzhou, Zhejiang,
                      China
                    </Descriptions.Item>
                  </Descriptions>
                ) : (
                  <div>Couldn't load data</div>
                )}
              </div>
            </Skeleton>
          </Card>
        </div>
        <div
          style={{
            flex: 2,
            flexDirection: "row"
          }}
          className="InnerWrapper"
        >
          <TabsContent />
        </div>
      </div>
    );
  }
  Appointments = val => {
    const data = this.state.appointments;
    return (
      <div>
        {data && data.length > 0 ? (
          data.map(datum => {
            return (
              <Descriptions
                size="small"
                title={"Appointment No" + datum.id}
                bordered
              >
                <Descriptions.Item label="Created Date">
                  {datum.createdDate}
                </Descriptions.Item>
                <Descriptions.Item label="Status">
                  <Badge status="processing" text={datum.status} />
                </Descriptions.Item>
                <Descriptions.Item span={2} label={"Appointment Date & Time"}>
                  {datum.appointmentDateTime}
                </Descriptions.Item>
                <Descriptions.Item span={2} label="Message">
                  {datum.message}
                </Descriptions.Item>
                <Descriptions.Item label="Actions">
                  <Button>Cancel</Button>
                </Descriptions.Item>
              </Descriptions>
            );
          })
        ) : this.state.appointmentLoading ? (
          <Spinner />
        ) : (
          <div>No Data</div>
        )}
      </div>
    );
  };

  ReservedVehicles = val => {
    const data = this.state.reservedVehicle;
    return (
      <div>
        {data && data.length > 0 ? (
          data.map(datum => {
            return (
              <Card
                style={{ width: 300 }}
                actions={[
                  <SettingOutlined key="setting" />,
                  <EditOutlined key="edit" />,
                  <EllipsisOutlined key="ellipsis" />
                ]}
              >
                <Skeleton loading={this.state.vehicleLoading}>
                  <Meta
                    // avatar={
                    //   <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                    // }
                    title={
                      datum.vehicleMake +
                      " " +
                      datum.vehicleModel +
                      " " +
                      datum.vehicleYom
                    }
                    description={"LKR: " + datum.vehiclePrice}
                  />
                </Skeleton>
              </Card>
            );
          })
        ) : (
          <div>No Data</div>
        )}
      </div>
    );
  };

  PostedVehicles = val => {
    return <div>{"PostedVehicles" + val}</div>;
  };

  getAppointmentData(type, token, email) {
    axios
      .get(BASE_URL + `api/appointments/getAppointmentByUser/${email}`, {
        headers: {
          Authorization: type + " " + token
        }
      })

      .then(res => {
        if (res.status === 200) {
          this.setState({
            appointments: res.data,
            appointmentLoading: false
          });
        }
      })
      .catch(err => {});
  }

  getReservedVehicles(type, token, email) {
    axios
      .get(BASE_URL + `api/vehicle/getReservedVehicles/${email}`, {
        headers: {
          Authorization: type + " " + token
        }
      })

      .then(res => {
        if (res.status === 200) {
          this.setState({
            reservedVehicle: res.data,
            vehicleLoading: false
          });
        }
      })
      .catch(err => {});
  }

  getMyAds(type, token, email) {
    axios
      .get(BASE_URL + `api/vehicle/getAllVehicleByUser/${email}`, {
        headers: {
          Authorization: type + " " + token
        }
      })

      .then(res => {
        if (res.status === 200) {
          this.setState({
            myAds: res.data,
            adLoading: false
          });
        }
      })
      .catch(err => {});
  }
  getuser(type, token, email) {
    axios
      .get(BASE_URL + `api/users/getUser/${email}`, {
        headers: {
          Authorization: type + " " + token
        }
      })

      .then(res => {
        if (res.status === 200) {
          this.setState({
            userDetails: res.data,
            userLoading: false
          });
        }
      })
      .catch(err => {});
  }

  getSavedList(type, token, id) {
    // "http://localhost:8080/api/savedlist/get/idreesna@hotmail.com"
    axios
      .get(BASE_URL + `api/savedlist/get/${id}`, {
        headers: {
          Authorization: type + " " + token
        }
      })
      .then(res => {
        if (res.status === 200) {
          this.setState({
            saveditems: res.data,
            savedLoading: false
          });
        }
      })
      .catch(err => {});
  }
}

export default UserDashboard;
