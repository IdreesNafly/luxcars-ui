import React, { Component } from "react";
import { Result, Button } from "antd";
import { Link } from "react-router-dom";

class Checkout extends Component {
  state = {};
  componentDidMount() {
    console.log("dd");
    console.log(this.props.location);
  }
  render() {
    return (
      <div
        style={{
          display: "flex",
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          marginTop: "10rem"
        }}
      >
        <Result
          status="success"
          title="You have Reserved the following Vehicle!"
          subTitle="Order number: 2017182818828182881. Please visit us within 2-3 working days to confirm your purchase."
          extra={[
            <Link
              to={{
                pathname: `/contactus`
                //state: { vehicleData: data }
              }}
            >
              <Button type="primary" key="console">
                Contact Us!
              </Button>
            </Link>,
            <Link
              to={{
                pathname: `/`
              }}
            >
              <Button key="buy">Go Home</Button>
            </Link>
          ]}
        />
      </div>
    );
  }
}

export default Checkout;
