import React, { Component } from "react";
import styled from "styled-components";
import Navbar from "../Components/Navbar";
import DropdownPage from "../Components/Dropdown";
import { Link } from "react-router-dom";
import axios from "axios";
import {
  MDBBtn,
  MDBIcon,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBCardTitle,
  MDBCardText,
  MDBCol,
  MDBInput
} from "mdbreact";
import { Input, BackTop, Button } from "antd";
import Spinner from "../Components/Spinner";
import { Select } from "antd";
import { SearchOutlined } from "@ant-design/icons";

const { Search } = Input;

const { Option } = Select;

function onBlur() {
  console.log("blur");
}

function onFocus() {
  console.log("focus");
}

function onSearch(val) {
  console.log("search:", val);
}

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  margin: 2rem;
`;
const SearchFormWrapper = styled.div`
  display: flex;
  display: flex;
  border-style: solid;
  align-items: center;
  flex-direction: column;
  flex: 1;
  height: 75vh;
  padding: 1rem;
  border-bottom: white;
  border-top: white;
  border-left: white;
  border-width: 5px;
  border-color: black;
`;
const Title = styled.div`
  display: flex;
  font-size: 1rem;
`;

const CarListWrapper = styled.div`
  display: flex;
  flex: 5;
  flex-wrap: wrap;
`;

const Header = styled.div`
  display: flex;
  height: 25vh;
  background-image: url("https://www.carmax.com/home/images/home/hero/desktop/default.jpg");
  background-size: cover;
  margin-bottom: 2rem;
  background-attachment: fixed;
  background-position-x: left;
`;

const Msg = styled.div`
  display: flex;
  align-self: center;
  flex: 1;
  justify-content: center;
`;
const CardItm = styled.div`
display: flex;
flex: 1 1 0%;
margin: auto;
justify-content: center;
align-items: center;
}
`;
const BASE_URL = "http://localhost:8080/";

class CarListing extends Component {
  state = {
    searchloading: false,
    loading: true,
    error: false,
    make: "",
    model: "",
    year: ""
  };
  onChangeMake = value => {
    console.log(`selected ${value}`);
    this.setState({
      make: value
    });
  };
  onChangeModel = value => {
    console.log(`selected ${value}`);
    this.setState({
      model: value
    });
  };
  onChangeYear = value => {
    console.log(`selected ${value}`);
    this.setState({
      year: value
    });
  };
  ViewAllVehicles = () => {
    let self = this;
    axios
      .get(BASE_URL + "api/vehicle/getResVehicle")
      .then(res => {
        self.setState({
          vehicleData: res.data,
          loading: false,
          error: false
        });
      })
      .catch(err => {
        self.setState({
          error: true,
          loading: false
        });
      });
  };

  componentDidMount() {
    const searchData = this.props.location.state;
    console.log(searchData);
    if (searchData) {
      Object.values(searchData).map(vehicle => {
        this.setState({
          vehicleData: vehicle
        });
      });
    } else {
      this.ViewAllVehicles();
    }

    this.getAllModel();
    this.getAllMake();
    this.getAllYear();
  }

  render() {
    return (
      <Wrapper>
        {/* <Navbar /> */}
        <Header>
          <Title
            style={{
              flex: 1,
              fontSize: "4rem",
              fontWeight: "bold",
              justifyContent: "center",
              alignItems: "center",
              color: "white"
            }}
          >
            Shop Cars
          </Title>
        </Header>
        <Search
          placeholder="input search text"
          enterButton="Search"
          size="large"
          onSearch={value => this.searchbar(value)}
        />
        <br />
        <div style={{ display: "flex", flexDirection: "row", flex: 1 }}>
          <SearchFormWrapper>
            <Title style={{ fontSize: "1.5rem" }}>Search Cars</Title>

            <Select
              showSearch
              style={{ width: 200, marginTop: "1rem", marginBottom: "1rem" }}
              placeholder="Select Vehicle Make"
              optionFilterProp="children"
              onSelect={this.onChangeMake}
              onFocus={onFocus}
              onBlur={onBlur}
              onSearch={onSearch}
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {this.state.vehicleMake &&
                this.state.vehicleMake.map(item => {
                  return <Option value={item}>{item}</Option>;
                })}
            </Select>
            <Select
              showSearch
              style={{ width: 200, marginTop: "1rem", marginBottom: "1rem" }}
              placeholder="Select Vehicle Model"
              optionFilterProp="children"
              onSelect={this.onChangeModel}
              onFocus={onFocus}
              onBlur={onBlur}
              onSearch={onSearch}
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {this.state.vehicleModel &&
                this.state.vehicleModel.map(item => {
                  return <Option value={item}>{item}</Option>;
                })}
            </Select>
            <Select
              showSearch
              style={{ width: 200, marginTop: "1rem", marginBottom: "1rem" }}
              placeholder="Select Vehicle Year"
              optionFilterProp="children"
              onSelect={this.onChangeYear}
              onFocus={onFocus}
              onBlur={onBlur}
              onSearch={onSearch}
              //onSelect={e => this.selectedVal(e)}
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {this.state.vehicleYear &&
                this.state.vehicleYear.map(item => {
                  return <Option value={item}>{item}</Option>;
                })}
            </Select>
            {/* <MDBInput
              label="Make"
              outline
              icon="car-alt"
              value={this.state.make}
              onChange={e => this.handleOnChangeMk(e)}
            />
            <MDBInput
              label="Model"
              outline
              icon="vr-cardboard"
              value={this.state.model}
              onChange={e => this.handleOnChangeMd(e)}
            />
            <MDBInput
              label="Year"
              outline
              icon="calendar-alt"
              value={this.state.year}
              onChange={e => this.handleOnChangeYr(e)}
            /> */}
            <Button
              style={{
                width: 200,
                marginTop: "1rem",
                marginBottom: "1rem",
                color: "black",
                borderColor: "black"
              }}
              onClick={e => this.search()}
              icon={<SearchOutlined />}
            >
              Search
            </Button>

            {/* <MDBBtn color="primary" onClick={e => this.search()}>
              <MDBIcon icon="search" className="mr-1" /> Search
            </MDBBtn> */}
          </SearchFormWrapper>
          <CarListWrapper>
            {this.state.vehicleData && this.state.vehicleData.length > 0 ? (
              this.state.vehicleData.map((vehicle, index) => {
                if (vehicle.status === "Available") {
                  // console.log(vehicle);
                  return (
                    <MDBCol style={{ marginBottom: "3rem" }}>
                      <MDBCard
                        style={{
                          minWidth: "22rem",
                          minHeight: "32rem",
                          maxWidth: "22rem",
                          maxHeight: "32rem"
                        }}
                      >
                        <MDBCardImage
                          className="img-fluid"
                          src={
                            vehicle.covrimg
                              ? vehicle.covrimg
                              : "https://www.driving.co.uk/s3/st-driving-prod/uploads/2019/11/2020-Mercedes-Benz-EQC-Jeremy-Clarkson-review-December-2019-01.jpg"
                          }
                          //alt="https://www.driving.co.uk/s3/st-driving-prod/uploads/2019/11/2020-Mercedes-Benz-EQC-Jeremy-Clarkson-review-December-2019-01.jpg"
                          waves
                        />
                        <MDBCardBody>
                          <MDBCardTitle>
                            {vehicle.vehicleMake + " " + vehicle.vehicleModel}
                          </MDBCardTitle>
                          <MDBCardText style={{ height: "7rem" }}>
                            {vehicle.vehicleDescription}
                          </MDBCardText>
                          <MDBCardText
                            style={{
                              height: "2rem",
                              flexDirection: "row",
                              display: "flex"
                            }}
                          >
                            <CardItm>
                              <i
                                style={{ marginRight: "1rem" }}
                                class="fas fa-cog"
                              ></i>
                              {vehicle.vehicleTransmission}
                            </CardItm>
                            <CardItm>
                              <i
                                style={{ marginRight: "1rem" }}
                                class="fas fa-calendar-alt"
                              ></i>
                              {vehicle.vehicleYom}
                            </CardItm>
                            <CardItm>
                              <i
                                style={{ marginRight: "1rem" }}
                                class="fas fa-tachometer-alt"
                              ></i>{" "}
                              {vehicle.vehicleMileage + " KM"}
                            </CardItm>
                          </MDBCardText>
                          <Link
                            to={{
                              pathname: `/carlisting/detailedcar/${vehicle.id}`,
                              state: { vehicleData: vehicle }
                            }}
                          >
                            <MDBBtn
                              style={{
                                width: "100%",
                                color: "white",
                                fontSize: "1rem",
                                lineHeight: 0.5,
                                fontVariantCaps: "all-petite-caps",
                                fontWeight: 500
                              }}
                            >
                              View More
                            </MDBBtn>
                          </Link>
                        </MDBCardBody>
                      </MDBCard>
                    </MDBCol>
                  );
                }
              })
            ) : this.state.loading ? (
              <Msg>
                <Spinner />
              </Msg>
            ) : this.state.error ? (
              <Msg> Error</Msg>
            ) : (
              <Msg> </Msg>
            )}
          </CarListWrapper>
        </div>
      </Wrapper>
    );
  }
  searchbar = value => {
    const data = this.state.vehicleData;

    const lowercasedFilter = value.toLowerCase();

    const excludeColumns = ["imageList", "id"];

    const FILTERED_DATA = data.filter(item => {
      return Object.keys(item).some(key =>
        excludeColumns.includes(key)
          ? false
          : item[key]
              .toString()
              .toLowerCase()
              .includes(lowercasedFilter)
      );
    });

    console.log(FILTERED_DATA);
    // this.setState({
    //   vehicleData: filteredData
    // });
  };

  //search

  selectedVal = e => {
    console.log(e);
  };
  search() {
    this.setState({
      loading: true
    });
    const make = this.state.make;
    const model = this.state.model;
    const year = this.state.year;
    axios
      .get(BASE_URL + `api/vehicle/search/${make}/${model}/${year}`)
      .then(res => {
        if (res.status === 200) {
          this.setState({
            loading: false,
            vehicleData: res.data
          });
        }
      })
      .catch(res => {});
  }
  // handleOnChangeMk(e) {
  //   this.setState({
  //     make: e.target.value
  //   });
  // }

  // handleOnChangeMd(e) {
  //   this.setState({
  //     model: e.target.value
  //   });
  // }

  // handleOnChangeYr(e) {
  //   this.setState({
  //     year: e.target.value
  //   });
  // }
  getAllModel = () => {
    axios
      .get(BASE_URL + "api/vehicle/getAllModel")
      .then(res => {
        this.setState({
          vehicleModel: Array.from(new Set(res.data))
        });
      })
      .catch(err => {});
  };
  getAllMake = () => {
    axios
      .get(BASE_URL + "api/vehicle/getAllMake")
      .then(res => {
        this.setState({
          vehicleMake: Array.from(new Set(res.data))
        });
      })
      .catch(err => {});
  };
  getAllYear = () => {
    axios
      .get(BASE_URL + "api/vehicle/getAllYear")
      .then(res => {
        this.setState({
          vehicleYear: Array.from(new Set(res.data))
        });
      })
      .catch(err => {});
  };
}
export default CarListing;
