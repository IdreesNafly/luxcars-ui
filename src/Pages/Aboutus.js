import React, { Component } from "react";
import { MDBRow, MDBCol, MDBCard, MDBIcon, MDBBtn } from "mdbreact";
import Review from "../Components/Reviews";
import styled from "styled-components";
import { Form, Input, Button, Avatar } from "antd";
import TextArea from "antd/lib/input/TextArea";
import moment from "moment";
import StarRatingComponent from "react-star-rating-component";
import Axios from "axios";
import { BASE_URL } from "../services";
import cogoToast from "cogo-toast";

const ReviewsWrapper = styled.div`
  display: flex;
  flex: 1;
  padding: 0 0 0 2rem;
  flex-direction: column;
`;

class Aboutus extends Component {
  state = {
    rating: 1,
    submitting: false,
    value: ""
  };
  componentDidMount() {
    const email = localStorage.getItem("email");
    const token = localStorage.getItem("token");
    this.setState({
      userid: email,
      token: token
    });
    this.getAllReviews();
  }
  render() {
    return (
      <div
        style={{
          display: "flex",
          flex: 1,
          justifyContent: "center",
          margin: "0rem 4rem 0 4rem",
          flexDirection: "column"
        }}
      >
        <h2
          style={{
            alignSelf: "center",
            marginTop: "2rem",
            marginBottom: "2rem"
          }}
          className="h1-responsive font-weight-bold"
        >
          About LuxCars
        </h2>
        {/* <p className="grey-text w-responsive mx-auto mb-5">
            Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
            cupidatat non proident, sunt in culpa qui officia deserunt mollit
            est laborum.
          </p> */}
        <section
          style={{ display: "flex", flexDirection: "row" }}
          className="text-center"
        >
          <MDBRow style={{ flex: 1 }}>
            <MDBCol md="12" className="mb-4">
              <MDBCard
                className="card-image"
                style={{
                  backgroundImage:
                    "url(https://mdbootstrap.com/img/Photos/Others/img%20%2832%29.jpg)"
                }}
              >
                <div className="text-white text-center d-flex flex-column align-items-center rgba-black-strong py-4 px-4 rounded">
                  <h6 className="purple-text">
                    <MDBIcon icon="plane" />
                    <strong> Customer Service</strong>
                  </h6>
                  <h3 className="py-1 font-weight-bold">
                    <strong>Guaranteed</strong>
                  </h3>
                  <p className="pb-1">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Repellat fugiat, laboriosam, voluptatem, optio vero odio nam
                    sit officia accusamus minus error nisi architecto nulla
                    ipsum dignissimos. Odit sed qui, dolorum!
                  </p>
                  {/* <MDBBtn color="secondary" rounded size="md">
                    <MDBIcon far icon="clone" className="left" /> MDBView
                    project
                  </MDBBtn> */}
                </div>
              </MDBCard>
            </MDBCol>
            <MDBCol md="6" className="md-0 mb-4">
              <MDBCard
                className="card-image"
                style={{
                  backgroundImage:
                    "url(https://mdbootstrap.com/img/Photos/Horizontal/Nature/6-col/img%20%2873%29.jpg)"
                }}
              >
                <div className="text-white text-center  d-flex align-items-center rgba-black-strong py-3 px-2 rounded">
                  <div>
                    <h6 className="pink-text">
                      <MDBIcon icon="chart-pie" />
                      <strong> Quality</strong>
                    </h6>
                    <h3 className="py-1 font-weight-bold">
                      <strong>Guaranteed</strong>
                    </h3>
                    <p className="pb-1">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Repellat fugiat, laboriosam, voluptatem, optio vero odio
                      nam sit officia accusamus minus error nisi architecto
                      nulla ipsum dignissimos. Odit sed qui, dolorum!
                    </p>
                    {/* <MDBBtn color="pink" rounded size="md">
                      <MDBIcon far icon="clone" className="left" /> MDBView
                      project
                    </MDBBtn> */}
                  </div>
                </div>
              </MDBCard>
            </MDBCol>
            <MDBCol md="6" className="md-0 mb-4">
              <MDBCard
                className="card-image"
                style={{
                  backgroundImage:
                    "url(https://mdbootstrap.com/img/Photos/Horizontal/Nature/6-col/img%20%2873%29.jpg)"
                }}
              >
                <div className="text-white text-center d-flex align-items-center rgba-black-strong py-3 px-3 rounded">
                  <div>
                    <h6 className="green-text">
                      <MDBIcon icon="eye" />
                      <strong> After sales service</strong>
                    </h6>
                    <h3 className="py-1 font-weight-bold">
                      <strong>Guaranteed</strong>
                    </h3>
                    <p className="pb-1">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Repellat fugiat, laboriosam, voluptatem, optio vero odio
                      nam sit officia accusamus minus error nisi architecto
                      nulla ipsum dignissimos. Odit sed qui, dolorum!
                    </p>
                    {/* <MDBBtn color="success" rounded size="md">
                      <MDBIcon far icon="clone" className="left" /> MDBView
                      project
                    </MDBBtn> */}
                  </div>
                </div>
              </MDBCard>
            </MDBCol>
          </MDBRow>

          <ReviewsWrapper>
            <div
              style={{
                flex: 2,
                overflow: "auto",
                border: "2px solid #1890ff",
                height: "45vh",
                padding: "1rem"
              }}
            >
              {this.state.allReviews &&
                this.state.allReviews.map(item => {
                  return (
                    <div
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        display: "flex",
                        borderBottom: "2px dotted",
                        paddingTop: "1rem"
                      }}
                    >
                      <Avatar
                        src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
                        alt="Han Solo"
                      />
                      <div
                        style={{
                          display: "flex",
                          flex: 1,
                          flexDirection: "column"
                        }}
                      >
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                            paddingLeft: "2rem"
                          }}
                        >
                          <span>{item.userid}</span>
                          <div>{"On : " + item.createdDate}</div>
                        </div>
                        <div
                          style={{
                            display: "flex",
                            paddingLeft: "2rem",
                            color: "black",
                            fontWeight: 500
                          }}
                        >
                          {item.description}
                        </div>
                        <div>
                          <StarRatingComponent
                            name="rate2"
                            editing={false}
                            //renderStarIcon={() => <span></span>}
                            starCount={5}
                            value={item.rating}
                          />
                        </div>
                      </div>
                    </div>
                  );
                })}
            </div>
            <div style={{ flex: 1, marginTop: "1rem" }}>
              <TextArea
                rows={3}
                onChange={e => this.handleChange(e)}
                value={this.state.value}
                placeholder="Enter your comment here..."
              />
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <StarRatingComponent
                  name="rate1"
                  starCount={5}
                  value={this.state.rating}
                  onStarClick={this.onStarClick.bind(this)}
                />
                <Button
                  style={{ marginTop: "1rem" }}
                  onClick={e => this.handleSubmit()}
                  type="primary"
                >
                  Leave a review
                </Button>
              </div>
            </div>
          </ReviewsWrapper>
        </section>
      </div>
    );
  }
  handleChange = e => {
    this.setState({
      value: e.target.value
    });
    console.log(e.target.value);
  };

  handleSubmit = () => {
    const description = this.state.value;
    const rating = this.state.rating;
    const userid = this.state.userid;
    const token = this.state.token;
    const payload = {
      description: description,
      rating: rating,
      userid: userid
    };

    this.addReview(payload, token);
  };

  addReview = (val, token) => {
    Axios.post(BASE_URL + "api/reviews/addReview", val, {
      headers: {
        Authorization: "Bearer " + token
      }
    })
      .then(res => {
        if (res.status === 200) {
          cogoToast.success("Review Submiited");
          this.componentDidMount();
        }
      })
      .catch(err => {
        cogoToast.error("Error");
      });
  };
  onStarClick(nextValue, prevValue, name) {
    this.setState({ rating: nextValue });
  }
  getAllReviews = () => {
    Axios.get(BASE_URL + "api/reviews/getAllReviews")
      .then(res => {
        if (res.status == 200) {
          this.setState({
            allReviews: res.data
          });
        }
      })
      .catch(Err => {});
  };
}

export default Aboutus;
