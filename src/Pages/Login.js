import React, { Component } from "react";
import {
  Form,
  Input,
  Button,
  Checkbox,
  message,
  Card,
  Divider,
  Alert
} from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import "../Stylesheets/PageStyles.css";
import axios from "axios";
import luxcars from "../Images/luxcars.jpg";
import { Link } from "react-router-dom";
import cogoToast from "cogo-toast";
const BASE_URL = "http://localhost:8080/";

// const validateMessages = {
//   required: '${label} is required!',
//   types: {
//     email: '${label} is not validate email!',
//     number: '${label} is not a validate number!',
//   },
//   number: {
//     range: '${label} must be between ${min} and ${max}',
//   },
// };

class Login extends Component {
  state = {
    isError: false,
    errorMsg: null
  };

  onFinish = values => {
    console.log("Received values of form: ", values);

    axios
      .post(BASE_URL + "api/auth/signin", values)
      .then(res => {
        if (res.status === 200) {
          message.success("Login Success");
          if (res.data.roles[0] === "ROLE_ADMIN") {
            this.props.history.push("/adminhome");
          } else if (res.data.roles[0] === "ROLE_USER") {
            this.props.history.push("/");
          }
          localStorage.setItem("token", res.data.token);
          localStorage.setItem("type", res.data.type);
          localStorage.setItem("email", res.data.email);
          localStorage.setItem("name", res.data.username);
          localStorage.setItem("role", res.data.roles[0]);
          localStorage.setItem("isAuth", true);
        }
      })
      .catch(err => {
        this.setState({
          isError: true,
          errorMsg: err.response.data.message || err.response.data
        });
        console.log(err.response);
        cogoToast.error(this.state.errorMsg);
      });
  };

  render() {
    return (
      <div className="loginWrapper">
        <Card
          style={{ width: "50%", height: "60%" }}
          bodyStyle={{ display: "flex", flexDirection: "row" }}
        >
          <div style={{ display: "flex", flex: 1, marginRight: "1.5rem" }}>
            <Link to="/">
              <img style={{ height: "20rem" }} src={luxcars} />
            </Link>
          </div>
          <Divider type="vertical" style={{ height: "20rem" }} />
          <div
            style={{
              flex: 1,
              display: "flex",
              justifyContent: "center",
              paddingTop: "4rem",
              flexDirection: "column"
            }}
          >
            <Form
              //validateMessages={validateMessages}
              name="normal_login"
              className="login-form"
              initialValues={{
                remember: true
              }}
              onFinish={this.onFinish}
            >
              <Form.Item
                name="email"
                label="Email"
                rules={[
                  {
                    required: true,
                    message: "Please input your Email!"
                  },
                  {
                    type: "email",
                    message: "Invalid Email Format"
                  }
                ]}
              >
                <Input
                  prefix={<UserOutlined className="site-form-item-icon" />}
                  placeholder="Email"
                />
              </Form.Item>
              <Form.Item
                name="password"
                label="Password"
                rules={[
                  {
                    required: true,
                    message: "Please input your Password!"
                  }
                ]}
                hasFeedback
              >
                <Input
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                  placeholder="Password"
                />
              </Form.Item>
              <Form.Item>
                <Form.Item name="remember" valuePropName="checked" noStyle>
                  <Checkbox>Remember me</Checkbox>
                </Form.Item>

                <a className="login-form-forgot" href="#">
                  Forgot password
                </a>
              </Form.Item>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="login-form-button"
                >
                  Log in
                </Button>
              </Form.Item>
              Or <a href="/Signup">register now!</a>
            </Form>
          </div>
        </Card>
      </div>
    );
  }
}

export default Login;
