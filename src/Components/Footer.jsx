import React from "react";
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";

const FooterPage = () => {
  return (
    <MDBFooter
      style={{ background: "linear-gradient(40deg, #4e3e47, #7873f5)" }}
      //color="linear-gradient(40deg, #4e3e47, #7873f5)"
      className="font-small pt-4 mt-4"
    >
      <MDBContainer fluid className="text-center text-md-left">
        <MDBRow>
          <MDBCol md="6">
            <h5 className="title">{"Luxcars International (Pvt) Ltd"}</h5>
            <p>
              All copyrights under Luxcars int. Developed and Maintained by
              Luxcars Int.
            </p>
          </MDBCol>
          <MDBCol md="6">
            <h5 className="title">Pages</h5>
            <ul>
              <li className="list-unstyled">
                <a href="/">Home</a>
              </li>
              <li className="list-unstyled">
                <a href="/carlisting">Buy Car</a>
              </li>
              <li className="list-unstyled">
                <a href="/sell-car">Sell-Car</a>
              </li>
              <li className="list-unstyled">
                <a href="/contactus">Contact Us</a>
              </li>
            </ul>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
      <div className="footer-copyright text-center py-3">
        <MDBContainer fluid>
          &copy; {new Date().getFullYear()} Copyright:{" "}
          <a href="https://www.facebook.com/luxcarsint/">
            {" "}
            Luxcars International
          </a>
        </MDBContainer>
      </div>
    </MDBFooter>
  );
};

export default FooterPage;
