import React, { Component } from "react";
import {
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavLink,
  MDBNavbarToggler,
  MDBCollapse,
  MDBContainer,
  MDBIcon,
  MDBBtn
} from "mdbreact";
import { BrowserRouter as Router, Link } from "react-router-dom";

class NavbarPage extends Component {
  state = {
    collapseID: ""
  };

  toggleCollapse = collapseID => () => {
    this.setState(prevState => ({
      collapseID: prevState.collapseID !== collapseID ? collapseID : ""
    }));
  };

  render() {
    return (
      <Router>
        <MDBContainer>
          <MDBNavbar color="blue lighten-4" style={{ marginTop: "20px" }} light>
            <MDBContainer>
              <a href="/adminhome">
                <MDBNavbarBrand>Luxars Admin</MDBNavbarBrand>
              </a>
              <div>
                {/* <MDBNavbarToggler
                  tag="button"
                  className="aqua-gradient"
                  onClick={this.toggleCollapse("navbarCollapse13")}
                >
                  <span className="white-text">
                    <MDBIcon icon="bars" />
                  </span>
                </MDBNavbarToggler> */}
                <a href="/adminhome">
                  <MDBBtn tag="a" size="sm" floating gradient="blue">
                    Home
                  </MDBBtn>
                </a>
                <MDBBtn
                  onClick={this.onLogout}
                  tag="a"
                  size="sm"
                  floating
                  gradient="blue"
                >
                  Logout
                </MDBBtn>
              </div>
              {/* <MDBCollapse
                id="navbarCollapse13"
                isOpen={this.state.collapseID}
                navbar
              >
                <MDBNavbarNav left>
                  <MDBNavItem active>
                    <MDBNavLink to="/adminhome">Home</MDBNavLink>
                  </MDBNavItem>
                  <MDBNavItem>
                    <MDBNavLink to="/manageappointments">
                      Manage Appointments
                    </MDBNavLink>
                  </MDBNavItem>
                  <MDBNavItem>
                    <MDBNavLink to="/manageads">Manage Ads</MDBNavLink>
                  </MDBNavItem>
                </MDBNavbarNav>
              </MDBCollapse> */}
            </MDBContainer>
          </MDBNavbar>
        </MDBContainer>
      </Router>
    );
  }

  onLogout = () => {
    localStorage.clear();
    window.location.href = "/";
  };
}

export default NavbarPage;
