import React, { Component } from "react";
import {
  MDBContainer,
  MDBTabPane,
  MDBTabContent,
  MDBNav,
  MDBNavItem,
  MDBNavLink
} from "mdbreact";
import axios from "axios";
import {
  EditOutlined,
  EllipsisOutlined,
  SettingOutlined,
  DeleteOutlined
} from "@ant-design/icons";
import {
  Card,
  Avatar,
  Divider,
  Descriptions,
  Badge,
  Button,
  message,
  Popconfirm
} from "antd";
import { UserOutlined } from "@ant-design/icons";
import { Tabs, Skeleton } from "antd";
import Spinner from "./Spinner";
import "../Stylesheets/UserDashboard.css";
import luxcars from "../Images/luxcars.jpg";
import cogoToast from "cogo-toast";
import "../Stylesheets/antd.css";
const BASE_URL = "http://localhost:8080/";
const { Meta } = Card;

const style = {
  width: "25vw",
  height: "25vh",
  display: "flex",
  flexDirection: "row",
  marginBottom: "1rem",
  marginRight: "1rem"
};
const bodystyle = {
  display: "flex",
  flex: 1,
  flexDirection: "row",
  padding: "13px"
};
class TabsContent extends Component {
  state = {
    activeItem: "1",
    appointmentLoading: true,
    vehicleLoading: true,
    adLoading: true,
    savedLoading: true
  };

  componentDidMount() {
    const token = localStorage.getItem("token");
    const type = localStorage.getItem("type");
    const mail = localStorage.getItem("email");

    this.setState({
      token: token,
      type: type,
      email: mail
    });

    this.getAppointmentData(type, token, mail);
    this.getMyAds(type, token, mail);
    this.getReservedVehicles(type, token, mail);
    this.getSavedList(type, token, mail);
  }

  toggle = tab => e => {
    if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
    }
  };

  render() {
    return (
      <MDBContainer style={{ margin: 0, maxWidth: "100vw" }}>
        <MDBNav className="nav-tabs mt-5">
          <MDBNavItem>
            <MDBNavLink
              link
              to="#"
              active={this.state.activeItem === "1"}
              onClick={this.toggle("1")}
              role="tab"
            >
              Appointments
            </MDBNavLink>
          </MDBNavItem>
          <MDBNavItem>
            <MDBNavLink
              link
              to="#"
              active={this.state.activeItem === "2"}
              onClick={this.toggle("2")}
              role="tab"
            >
              Reserved Vehicle
            </MDBNavLink>
          </MDBNavItem>
          <MDBNavItem>
            <MDBNavLink
              link
              to="#"
              active={this.state.activeItem === "3"}
              onClick={this.toggle("3")}
              role="tab"
            >
              My Ads
            </MDBNavLink>
          </MDBNavItem>
          <MDBNavItem>
            <MDBNavLink
              link
              to="#"
              active={this.state.activeItem === "4"}
              onClick={this.toggle("4")}
              role="tab"
            >
              Saved List
            </MDBNavLink>
          </MDBNavItem>
        </MDBNav>
        <MDBTabContent
          style={{ overflowY: "auto", height: "40vh" }}
          activeItem={this.state.activeItem}
        >
          <MDBTabPane tabId="1" role="tabpanel">
            <div className="TabWrapper">{this.Appointments()}</div>
          </MDBTabPane>
          <MDBTabPane tabId="2" role="tabpanel">
            <div className="TabWrapper">{this.ReservedVehicles()}</div>
          </MDBTabPane>
          <MDBTabPane tabId="3" role="tabpanel">
            <div className="TabWrapper">{this.PostedVehicles()}</div>
          </MDBTabPane>
          <MDBTabPane tabId="4" role="tabpanel">
            <div className="TabWrapper">{this.SavedList()}</div>
          </MDBTabPane>
        </MDBTabContent>
      </MDBContainer>
    );
  }

  SavedList = () => {
    // const data = this.state.saveditems.map(item => {
    //   return item.vehicle;
    // });
    const data = this.state.saveditems;

    if (data) {
      const saved = data.map(item => {
        return item.vehicle;
      });
      return (
        <div>
          {data
            ? data.map(vehicle => {
                return (
                  <Card
                    hoverable
                    style={{
                      display: "flex",
                      flex: 1,
                      background: "white",
                      border: " 1px solid darkblue",
                      marginBottom: "1rem",
                      maxHeight: "12rem"
                    }}
                    actions={[
                      <Popconfirm
                        placement="topRight"
                        title={
                          "Are you sure you want to remove from savedList?"
                        }
                        onConfirm={e => {
                          this.removeFromSavedList(vehicle.id);
                        }}
                        okText="Yes"
                        cancelText="No"
                      >
                        <DeleteOutlined
                          key="setting"
                          // onClick={e => this.deleteVehicle(datum.id)}
                        />
                      </Popconfirm>
                    ]}
                    bodyStyle={bodystyle}
                  >
                    <Skeleton loading={this.state.savedLoading}>
                      <div style={{ display: "flex", flex: 1 }}>
                        <img
                          style={{
                            maxHeight: "6rem",
                            maxWidth: "6rem",
                            minHeight: "6rem",
                            minWidth: "6rem"
                          }}
                          alt={luxcars}
                          src={
                            vehicle.vehicle.covrimg
                              ? vehicle.vehicle.covrimg
                              : luxcars
                          }
                        />
                      </div>
                      <Divider
                        style={{
                          height: "100%",
                          width: "3px",
                          background: "darkblue"
                        }}
                        type="vertical"
                      />
                      <div style={{ display: "flex", flex: 9 }}>
                        <Descriptions>
                          <Descriptions.Item label="Vehicle Make">
                            {vehicle.vehicle.vehicleMake}
                          </Descriptions.Item>
                          <Descriptions.Item label="Vehicle Model">
                            {vehicle.vehicle.vehicleModel}
                          </Descriptions.Item>
                          <Descriptions.Item label="YOM">
                            {vehicle.vehicle.vehicleYom}
                          </Descriptions.Item>
                          <Descriptions.Item label="Price">
                            {vehicle.vehicle.vehiclePrice}
                          </Descriptions.Item>

                          <Descriptions.Item label="Status">
                            {vehicle.vehicle.status}
                          </Descriptions.Item>
                          <Descriptions.Item label="Excterior Color">
                            {vehicle.vehicle.vehicleExteriorColor}
                          </Descriptions.Item>
                        </Descriptions>
                      </div>
                    </Skeleton>
                  </Card>
                );
              })
            : ""}
        </div>
      );
    }
  };
  Appointments = val => {
    const data = this.state.appointments;
    return (
      <div style={{ display: "flex", flex: 1, flexDirection: "column" }}>
        {data && data.length > 0 ? (
          data.map(datum => {
            var appointmentdate = new Date(
              datum.appointmentDateTime
            ).toLocaleString();
            var createddate = new Date(datum.createdDate).toLocaleString();

            return (
              <Descriptions
                size="small"
                title={"Appointment No" + datum.id}
                bordered
                column={2}
              >
                <Descriptions.Item
                  style={{ width: "15%" }}
                  label={"Appointment Date & Time"}
                >
                  {appointmentdate}
                </Descriptions.Item>
                <Descriptions.Item label="Created Date">
                  {createddate}
                </Descriptions.Item>
                <Descriptions.Item label="Status">
                  <Badge
                    status={
                      datum.status === "Confirmed" ? "processing" : "error"
                    }
                    text={datum.status}
                  />
                </Descriptions.Item>

                <Descriptions.Item span={2} label="Message">
                  {datum.message}
                </Descriptions.Item>
                {datum.status && datum.status === "Pending" ? (
                  <Descriptions.Item label="Actions">
                    <Popconfirm
                      placement="topRight"
                      title={
                        "Are you sure you want to Cancel this Appointment?"
                      }
                      onConfirm={e => {
                        this.cancelAppointment(datum.id);
                      }}
                      okText="Yes"
                      cancelText="No"
                    >
                      <Button>Cancel</Button>
                    </Popconfirm>
                  </Descriptions.Item>
                ) : (
                  ""
                )}
                {/* <Button
                    onClick={e => {
                      this.cancelAppointment(datum.id);
                    }}
                  >
                    Cancel
                  </Button> */}
              </Descriptions>
            );
          })
        ) : this.state.appointmentLoading ? (
          <Spinner />
        ) : (
          <div>No Data to Display</div>
        )}
      </div>
    );
  };

  cancelAppointment = id => {
    axios
      .put(BASE_URL + `api/appointments/cancelAppointment/${id}`, null, {
        headers: {
          Authorization: this.state.type + " " + this.state.token
        }
      })
      .then(res => {
        if (res.status === 200) {
          cogoToast.success("Your Appointment has been cancelled!");
        }
      })
      .catch(err => {
        cogoToast.error("Error");
      });
  };

  removeFromSavedList(id) {
    axios
      .delete(BASE_URL + `api/savedlist/deleteItem/${id}`, {
        headers: {
          Authorization: this.state.type + " " + this.state.token
        }
      })
      .then(res => {
        if (res.status === 200) {
          cogoToast.success("Item Removed");
          this.componentDidMount();
        }
      })
      .catch(err => {
        cogoToast.error("Error");
      });
  }

  ReservedVehicles = val => {
    const data = this.state.reservedVehicle;
    return (
      <div className="Vehicle">
        {data && data.length > 0 ? (
          data.map(datum => {
            return (
              <Card
                style={style}
                // cover={
                //   <img
                //     style={{ height: "100%" }}
                //     alt="example"
                //     src={datum.covrimg ? datum.covrimg : luxcars}
                //   />
                // }
              >
                <Skeleton loading={this.state.vehicleLoading} avatar active>
                  {/* <Meta
                    // avatar={
                    //   <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                    // }
                    title={
                      datum.vehicleMake +
                      " " +
                      datum.vehicleModel +
                      " " +
                      datum.vehicleYom
                    }
                    description={"LKR: " + datum.vehiclePrice}
                  />
                  <Badge
                    style={{ marginTop: "1rem" }}
                    status="processing"
                    text={datum.status}
                  /> */}
                  <div style={{ display: "flex" }}>
                    <img
                      style={{ height: "20vh", width: "10vw" }}
                      alt="example"
                      src={datum.covrimg ? datum.covrimg : luxcars}
                    />
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        padding: "1rem"
                      }}
                    >
                      <div style={{ color: "black", fontWeight: "bold" }}>
                        {datum.vehicleMake +
                          " " +
                          datum.vehicleModel +
                          " " +
                          datum.vehicleYom}
                      </div>
                      <div>{"LKR: " + datum.vehiclePrice}</div>
                      <Badge
                        style={{ marginTop: "1rem" }}
                        status="processing"
                        text={datum.status}
                      />
                    </div>
                  </div>
                </Skeleton>
              </Card>
            );
          })
        ) : (
          <div>No Data to Display</div>
        )}
      </div>
    );
  };

  PostedVehicles = val => {
    const data = this.state.myAds;
    return (
      <div className="Vehicle">
        {data && data.length > 0 ? (
          data.map(datum => {
            if (datum.isAdApproved) {
              return (
                <Card
                  style={style}
                  bodyStyle={{ width: "100%" }}
                  // cover={
                  //   <img
                  //     style={{ height: "100%" }}
                  //     src={datum.covrimg ? datum.covrimg : luxcars}
                  //     alt=""
                  //   />
                  // }
                  actions={[
                    <Popconfirm
                      placement="topRight"
                      title={"Are you sure you want to Delete this Ad?"}
                      onConfirm={e => {
                        this.deleteVehicle(datum.id);
                      }}
                      okText="Yes"
                      cancelText="No"
                    >
                      <DeleteOutlined
                        key="setting"
                        // onClick={e => this.deleteVehicle(datum.id)}
                      />
                    </Popconfirm>,
                    <Popconfirm
                      placement="topRight"
                      title={"Are you sure you want to Mark this Ad as Sold?"}
                      onConfirm={e => {
                        this.onUpdateVehicle(datum.id);
                      }}
                      okText="Yes"
                      cancelText="No"
                    >
                      <EditOutlined
                        key="setting"
                        // onClick={e => this.deleteVehicle(datum.id)}
                      />
                    </Popconfirm>
                    // <DeleteOutlined
                    //   key="setting"
                    //   onClick={e => this.deleteVehicle(datum.id)}
                    // />,
                    // <EditOutlined
                    //   key="edit"
                    //   onClick={e => this.onUpdateVehicle(datum.id)}
                    // />
                  ]}
                >
                  <Skeleton loading={this.state.vehicleLoading} avatar active>
                    {/* <Meta
                      title={
                        datum.vehicleMake +
                        " " +
                        datum.vehicleModel +
                        " " +
                        datum.vehicleYom
                      }
                      description={"LKR: " + datum.vehiclePrice}
                    />
                    <Badge
                      style={{ marginTop: "1rem" }}
                      status="processing"
                      text={datum.status}
                    /> */}
                    <div style={{ display: "flex" }}>
                      <img
                        style={{ height: "20vh", width: "10vw" }}
                        alt="example"
                        src={datum.covrimg ? datum.covrimg : luxcars}
                      />
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          padding: "1rem"
                        }}
                      >
                        <div style={{ color: "black", fontWeight: "bold" }}>
                          {datum.vehicleMake +
                            " " +
                            datum.vehicleModel +
                            " " +
                            datum.vehicleYom}
                        </div>
                        <div>{"LKR: " + datum.vehiclePrice}</div>
                        <Badge
                          style={{ marginTop: "1rem" }}
                          status="processing"
                          text={datum.status}
                        />
                      </div>
                    </div>
                  </Skeleton>
                </Card>
              );
            }
          })
        ) : (
          <div>No Data to Display</div>
        )}
      </div>
    );
  };

  deleteVehicle = id => {
    let self = this;
    axios
      .delete(BASE_URL + `api/vehicle/deleteVehicle/${id}`, {
        headers: {
          Authorization: this.state.type + " " + this.state.token
        }
      })
      .then(function(res) {
        if (res.status === 200) {
          cogoToast.success("Vehicle Deleted");
          self.componentDidMount();
        }
      })
      .catch(err => {
        cogoToast.error("Cannot Delete Vehicle");
      });
  };
  onUpdateVehicle = id => {
    const payload = {
      vehicleId: id,
      status: "Sold"
    };
    axios
      .put(BASE_URL + "api/vehicle/updateVehicleStatus", payload, {
        headers: {
          Authorization: this.state.type + " " + this.state.token
        }
      })
      .then(res => {
        if (res.status === 200) {
          cogoToast.success("Vehicle Status Updated as Sold");
        }
      })
      .catch(err => {
        cogoToast.error("Error");
      });
  };
  getAppointmentData(type, token, email) {
    axios
      .get(BASE_URL + `api/appointments/getAppointmentByUser/${email}`, {
        headers: {
          Authorization: type + " " + token
        }
      })

      .then(res => {
        if (res.status === 200) {
          this.setState({
            appointments: res.data,
            appointmentLoading: false
          });
        }
      })
      .catch(err => {});
  }

  getReservedVehicles(type, token, email) {
    axios
      .get(BASE_URL + `api/vehicle/getReservedVehicles/${email}`, {
        headers: {
          Authorization: type + " " + token
        }
      })

      .then(res => {
        if (res.status === 200) {
          this.setState({
            reservedVehicle: res.data,
            vehicleLoading: false
          });
        }
      })
      .catch(err => {});
  }

  getMyAds(type, token, email) {
    axios
      .get(BASE_URL + `api/vehicle/getAllVehicleByUser/${email}`, {
        headers: {
          Authorization: type + " " + token
        }
      })

      .then(res => {
        if (res.status === 200) {
          this.setState({
            myAds: res.data,
            adLoading: false
          });
        }
      })
      .catch(err => {});
  }

  getSavedList(type, token, id) {
    // "http://localhost:8080/api/savedlist/get/idreesna@hotmail.com"
    axios
      .get(BASE_URL + `api/savedlist/get/${id}`, {
        headers: {
          Authorization: type + " " + token
        }
      })
      .then(res => {
        if (res.status === 200) {
          this.setState({
            saveditems: res.data,
            savedLoading: false
          });
        }
      })
      .catch(err => {});
  }
}
export default TabsContent;
