import React, { Component } from "react";
import ChatBot from "react-simple-chatbot";
import luxcars from "../Images/luxcars.jpg";
class Chatbot extends Component {
  state = {};
  render() {
    return (
      <div>
        <ChatBot
          floating
          botAvatar={luxcars}
          steps={[
            {
              id: "1",
              message: "What is your name?",
              trigger: "2"
            },
            {
              id: "2",
              user: true,
              trigger: "3"
            },
            {
              id: "3",
              message: "Hi {previousValue}, nice to meet you!",
              end: true
            }
          ]}
        />
      </div>
    );
  }
}

export default Chatbot;
