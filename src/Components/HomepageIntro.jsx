import React, { Component } from "react";
import { BrowserRouter as Router, Link, withRouter } from "react-router-dom";
import {
  MDBMask,
  MDBRow,
  MDBCol,
  MDBIcon,
  MDBBtn,
  MDBView,
  MDBContainer,
  MDBCard,
  MDBCardBody,
  MDBInput
} from "mdbreact";
import "../Stylesheets/HomepageIntro.css";
import Axios from "axios";
import { BASE_URL } from "../services";

class ClassicFormPage extends Component {
  state = {
    loading: true,
    make: "",
    model: "",
    year: ""
  };

  handleOnChangeMk(e) {
    this.setState({
      make: e.target.value
    });
  }

  handleOnChangeMd(e) {
    this.setState({
      model: e.target.value
    });
  }

  handleOnChangeYr(e) {
    this.setState({
      year: e.target.value
    });
  }
  render() {
    return (
      <div id="classicformpage">
        <MDBView>
          <MDBMask className="d-flex justify-content-center align-items-center gradient">
            <MDBContainer>
              <MDBRow>
                <div className="white-text text-center text-md-left col-md-6 mt-xl-5 mb-5">
                  <h1 className="h1-responsive font-weight-bold">
                    The way it should be{" "}
                  </h1>
                  <hr className="hr-light" />
                  <h6 className="mb-4">
                    Leading online platform for buying and selling vehices
                    hassle-free
                  </h6>
                  <Link to="/carlisting">
                    <MDBBtn outline color="white">
                      BROWSE VEHICLES
                    </MDBBtn>
                  </Link>
                </div>
                <MDBCol md="6" xl="5" className="mb-4">
                  <MDBCard id="classic-card">
                    <MDBCardBody className="z-depth-2 white-text">
                      <h3 className="text-center">
                        <MDBIcon icon="car" /> Search Car
                      </h3>
                      <hr className="hr-light" />
                      <MDBInput
                        label="Vehicle Make"
                        icon="caret-down"
                        value={this.state.make}
                        onChange={e => this.handleOnChangeMk(e)}
                      />
                      <MDBInput
                        label="Vehicle Model"
                        icon="caret-down"
                        value={this.state.model}
                        onChange={e => this.handleOnChangeMd(e)}
                      />
                      <MDBInput
                        label="Vehicle Type"
                        icon="caret-down"
                        value={this.state.year}
                        onChange={e => this.handleOnChangeYr(e)}
                      />
                      <div className="text-center mt-4 black-text">
                        <MDBBtn onClick={e => this.search()} color="indigo">
                          Search
                        </MDBBtn>
                        <hr className="hr-light" />
                        <div className="text-center d-flex justify-content-center white-label">
                          <a href="#!" className="p-2 m-2">
                            <MDBIcon
                              fab
                              icon="twitter"
                              className="white-text"
                            />
                          </a>
                          <a href="#!" className="p-2 m-2">
                            <MDBIcon
                              fab
                              icon="linkedin-in"
                              className="white-text"
                            />
                          </a>
                          <a href="#!" className="p-2 m-2">
                            <MDBIcon
                              fab
                              icon="instagram"
                              className="white-text"
                            />
                          </a>
                        </div>
                      </div>
                    </MDBCardBody>
                  </MDBCard>
                </MDBCol>
              </MDBRow>
            </MDBContainer>
          </MDBMask>
        </MDBView>

        <MDBContainer>
          <MDBRow className="py-5">
            <MDBCol md="12" className="text-center"></MDBCol>
          </MDBRow>
        </MDBContainer>
      </div>
    );
  }

  search() {
    const make = this.state.make;
    const model = this.state.model;
    const year = this.state.year;
    Axios.get(BASE_URL + `api/vehicle/search/${make}/${model}/${year}`)
      .then(res => {
        if (res.status === 200) {
          this.props.history.push({
            pathname: "/carlisting",
            state: { searchdata: res.data }
          });
          this.setState({
            loading: false
          });
        }
      })
      .catch(res => {});
  }
}

export default withRouter(ClassicFormPage);
