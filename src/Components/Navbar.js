import React, { Component } from "react";
import {
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavbarToggler,
  MDBCollapse,
  MDBContainer
} from "mdbreact";
import { BrowserRouter as Router, Link, withRouter } from "react-router-dom";
import styled from "styled-components";
import { Avatar, Menu, Dropdown, Button, message, Tooltip } from "antd";
import { DownOutlined, UserOutlined } from "@ant-design/icons";
import AdminNavbar from "./AdminNavbar";

function handleMenuClick(e) {
  if (e.key === "1") {
    window.location.href = "/userdashboard";
  } else {
    localStorage.clear();
    window.location.href = "/";
  }
}

const menu = (
  <Menu onClick={handleMenuClick}>
    <Menu.Item key="1">
      <UserOutlined />
      User Dashboard
    </Menu.Item>
    <Menu.Item key="2">
      <UserOutlined />
      Log out
    </Menu.Item>
  </Menu>
);

const StyledLink = styled.a`
  color: black;
  font-weight: bold;
  margin-right: 1rem;
`;

class Navbar extends Component {
  state = {};
  componentDidMount() {
    const TOKEN = localStorage.getItem("token");
    const EMAIL = localStorage.getItem("email");
    const NAME = localStorage.getItem("name");
    const isAuth = localStorage.getItem("isAuth");
    const ROLE = localStorage.getItem("role");

    this.setState({
      token: TOKEN,
      email: EMAIL,
      name: NAME,
      role: ROLE,
      isAuth: isAuth
    });

    console.log(isAuth, TOKEN);
  }
  render() {
    const navStyle = { marginTop: "", backgroundColor: "white" };
    const overlay = (
      <div
        id="sidenav-overlay"
        style={{ backgroundColor: "transparent" }}
        onClick={this.handleTogglerClick}
      />
    );

    return (
      <Router>
        <div style={{ display: "flex", flex: 1 }}>
          {(this.state.role && this.state.role === "ROLE_ADMIN") ||
          window.location.href.includes("admin") ? (
            <AdminNavbar />
          ) : (
            <MDBNavbar
              color="#343a40 !important"
              style={navStyle}
              dark
              expand="md"
              fixed="top"
              scrolling
              transparent
            >
              <MDBContainer>
                <MDBNavbarBrand>
                  <Link to="/">
                    <strong className="black-text">
                      {"LuxCars International"}
                    </strong>
                  </Link>
                </MDBNavbarBrand>
                <MDBNavbarToggler onClick={this.handleTogglerClick} />
                {this.props.location.pathname === "/login" ||
                this.props.location.pathname == "/Signup" ? (
                  ""
                ) : (
                  <MDBCollapse isOpen={this.state.collapsed} navbar>
                    <MDBNavbarNav left>
                      <MDBNavItem active>
                        <StyledLink href="/">Home</StyledLink>
                      </MDBNavItem>
                      <MDBNavItem>
                        <StyledLink href="/carlisting">Buy Car</StyledLink>
                      </MDBNavItem>

                      {
                        <MDBNavItem>
                          <StyledLink
                            href={this.state.isAuth ? "/sell-car" : "/login"}
                          >
                            Sell Car
                          </StyledLink>
                        </MDBNavItem>
                      }

                      <MDBNavItem>
                        <StyledLink href="/About">About Us</StyledLink>
                      </MDBNavItem>
                      <MDBNavItem>
                        <StyledLink href="/Contact">Contact Us</StyledLink>
                      </MDBNavItem>
                    </MDBNavbarNav>

                    <MDBNavbarNav right>
                      {!this.state.isAuth ? (
                        <div style={{ display: "flex", flexDirection: "row" }}>
                          <MDBNavItem>
                            <StyledLink href="/login">Sign In</StyledLink>
                          </MDBNavItem>
                          <MDBNavItem>
                            <StyledLink href="/Signup">Sign Up</StyledLink>
                          </MDBNavItem>
                        </div>
                      ) : (
                        <MDBNavItem>
                          <Dropdown.Button
                            overlay={menu}
                            icon={<UserOutlined />}
                          >
                            {this.state.name}
                          </Dropdown.Button>
                        </MDBNavItem>
                      )}
                    </MDBNavbarNav>
                  </MDBCollapse>
                )}
              </MDBContainer>
            </MDBNavbar>
          )}
          {this.state.collapsed && overlay}
        </div>
      </Router>
    );
  }
}

export default withRouter(Navbar);
