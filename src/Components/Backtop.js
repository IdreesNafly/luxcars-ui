import { BackTop } from "antd";
import React, { Component } from "react";
import "../Stylesheets/PageStyles.css";

class Backtop extends Component {
  state = {};
  render() {
    return (
      <div>
        <BackTop>
          <div className="ant-back-top-inner">UP</div>
        </BackTop>
        Scroll down to see the bottom-right
        <strong style={{ color: "#1088e9" }}> blue </strong>
        button.
      </div>
    );
  }
}

export default Backtop;
