import React from "react";
import {
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem
} from "mdbreact";

const DropdownPage = () => {
  return (
    <MDBDropdown>
      <MDBDropdownToggle caret color="primary">
        MDBDropdown
      </MDBDropdownToggle>
      <MDBDropdownMenu basic>
        <MDBDropdownItem>Regular link</MDBDropdownItem>
        <MDBDropdownItem active href="#">
          Acrive link
        </MDBDropdownItem>
        <MDBDropdownItem href="#">Another link</MDBDropdownItem>
      </MDBDropdownMenu>
    </MDBDropdown>
  );
};

export default DropdownPage;
