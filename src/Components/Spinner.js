import React, { Component } from "react";

class Spinner extends Component {
  state = {};
  render() {
    return (
      <div className="spinner-grow text-success" role="status">
        <span
          style={{ display: "flex", fontSize: "1.5rem", color: "black" }}
          className="sr-only"
        >
          Loading...
        </span>
      </div>
    );
  }
}

export default Spinner;
