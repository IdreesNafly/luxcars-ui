import React, { Component } from "react";
import { Table, Radio, Divider, InputNumber, message } from "antd";
import styled from "styled-components";
import {
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBInput,
  MDBBtn,
  MDBTable,
  MDBTableBody,
  MDBTableHead,
  MDBDataTable
} from "mdbreact";
import axios from "axios";
import { BASE_URL } from "../services";
import { Modal, Button } from "antd";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import cogoToast from "cogo-toast";
const { confirm } = Modal;

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  margin: 0 4rem 4rem 4rem;
`;

class ManageVehicles extends Component {
  state = {
    loading: true,
    error: false,
    visible: false
  };

  state = { visible: false };

  showModal = () => {
    this.setState({
      visible: true
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false
    });
    let price = this.state.updatedPrice;
    this.UpdateVehiclePrice(price);
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  showDeleteConfirm(val) {
    confirm({
      title: "Are you sure delete this Vehicle?",
      icon: <ExclamationCircleOutlined />,
      content: "Vehicle will be removed from the system!!!",
      okText: "Delete",
      okType: "danger",
      cancelText: "No",
      onOk() {
        console.log("OK");
        val();
      },
      onCancel() {
        console.log("Cancel");
      }
    });
  }
  getData() {
    const data = {
      columns: this.getColumns(),
      rows: this.getRowData()
    };
    return data;
  }
  getColumns() {
    const columns = [
      {
        label: (
          <MDBInput
            label=" "
            type="checkbox"
            id="checkbox5"
            style={{ fontSize: "5px", marginLeft: "5rem" }}
          />
        ),
        field: "check",
        sort: "asc"
      },
      {
        label: "Vehicle Id",
        field: "id",
        sort: "asc"
      },
      {
        label: "Vehicle Make",
        field: "vehicleMake",
        sort: "asc"
      },
      {
        label: "vehicle Model",
        field: "vehicleModel",
        sort: "asc"
      },
      {
        label: "vehicle Type",
        field: "vehicleType",
        sort: "asc"
      },
      {
        label: "Vehicle YOM",
        field: "vehicleYom",
        sort: "asc"
      },
      {
        label: "Fuel",
        field: "vehicleFuel",
        sort: "asc"
      },
      {
        label: "Mileage",
        field: "vehicleMileage",
        sort: "asc"
      },
      {
        label: "Vehicle Price",
        field: "vehiclePrice",
        sort: "asc"
      },
      {
        label: "Status",
        field: "status",
        sort: "asc"
      }
    ];
    return columns;
  }
  componentDidMount() {
    const token = localStorage.getItem("token");
    const type = localStorage.getItem("type");

    this.setState({
      token: token,
      type: type
    });

    this.ViewAllVehicles();
  }
  onPriceChange = value => {
    console.log("changed", value);
    this.setState({
      updatedPrice: value
    });
  };
  render() {
    // this.transformTableColumn();
    return (
      <Wrapper>
        <Modal
          title="Update Vehicle Price"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <InputNumber
            style={{ width: "100%" }}
            onChange={this.onPriceChange}
            size="large"
            placeholder="Enter updated vehicle price"
          />
        </Modal>
        <MDBCard narrow>
          <MDBCardHeader className="view view-cascade gradient-card-header blue-gradient d-flex justify-content-between align-items-center py-2 mx-4 mb-3">
            <div>
              <MDBBtn outline rounded size="sm" color="white" className="px-2">
                <i className="fa fa-th-large mt-0"></i>
              </MDBBtn>
              <MDBBtn outline rounded size="sm" color="white" className="px-2">
                <i className="fa fa-columns mt-0"></i>
              </MDBBtn>
            </div>
            <a href="#" className="white-text mx-3">
              All Vehicles
            </a>
            <div>
              <MDBBtn
                onClick={this.showModal}
                outline
                rounded
                size="sm"
                color="white"
                className="px-2"
              >
                <i className="fas fa-pencil-alt mt-0"></i>
              </MDBBtn>
              <MDBBtn
                onClick={e => this.showDeleteConfirm(this.handleOnClickDelete)}
                outline
                rounded
                size="sm"
                color="white"
                className="px-2"
              >
                <i className="fas fa-times mt-0"></i>
              </MDBBtn>
              <MDBBtn outline rounded size="sm" color="white" className="px-2">
                <i className="fa fa-info-circle mt-0"></i>
              </MDBBtn>
            </div>
          </MDBCardHeader>
          <MDBCardBody cascade>
            <MDBDataTable paging={true} data={this.getData()} btn fixed>
              {/* <MDBTableHead columns={this.getColumns()} />
              <MDBTableBody rows={this.getRowData()} /> */}
            </MDBDataTable>
          </MDBCardBody>
        </MDBCard>
      </Wrapper>
    );
  }
  UpdateVehiclePrice = value => {
    const payload = {
      vehicleId: this.state.selectedRow,
      vehiclePrice: value
    };
    axios
      .put(BASE_URL + "api/vehicle/updateVehiclePrice", payload, {
        headers: {
          Authorization: this.state.type + " " + this.state.token
        }
      })

      .then(res => {
        let data = res.data;

        cogoToast.success("Vehicle Price updated!");
        this.componentDidMount();
      })
      .catch(err => {
        cogoToast.error("Error updating the price!");
      });
  };
  handleChange = val => {
    console.log(val);
    this.setState({
      selectedRow: val
    });
  };
  handleOnClickDelete = val => {
    let id = this.state.selectedRow;
    console.log(id, "id");
    this.deleteVehicle(id);
  };
  getRowData() {
    let data = this.state.vehicleData;

    let rows = [];
    if (data) {
      data.map(datum => {
        if (datum.isAd !== true) {
          let row = {
            check: (
              <MDBInput
                style={{ fontSize: "5px", marginLeft: "5rem" }}
                label={""}
                type="checkbox"
                id="checkbox6"
                value={datum.id}
                onChange={e => this.handleChange(datum.id)}
              />
            ),
            id: datum.id,
            vehicleMake: datum.vehicleMake,
            vehicleModel: datum.vehicleModel,
            vehicleType: datum.vehicleType,
            vehicleYom: datum.vehicleYom,
            vehicleFuel: datum.vehicleFuel,
            vehicleMileage: datum.vehicleMileage,
            vehiclePrice: datum.vehiclePrice,
            status: datum.status
          };
          rows.push(row);
        }
      });
    }
    console.log(rows);
    return rows;
  }
  deleteVehicle(id) {
    let self = this;
    axios
      .delete(BASE_URL + `api/vehicle/deleteVehicle/${id}`, {
        headers: {
          Authorization: this.state.type + " " + this.state.token
        }
      })
      .then(function(res) {
        if (res.status === 200) {
          cogoToast.success("Vehicle Deleted");
          self.componentDidMount();
        }
      })
      .catch(err => {
        cogoToast.error("Error Deleting vehicle");
      });
  }

  ViewAllVehicles = () => {
    let self = this;
    axios
      .get(BASE_URL + "api/vehicle/getAllVehicles")
      .then(res => {
        self.setState({
          vehicleData: res.data,
          loading: false,
          error: false
        });
      })
      .catch(err => {
        self.setState({
          error: true,
          loading: false
        });
      });
  };
}

export default ManageVehicles;
