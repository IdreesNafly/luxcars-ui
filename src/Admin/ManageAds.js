import React, { Component } from "react";
import styled from "styled-components";
import {
  Card,
  Divider,
  Button,
  Tooltip,
  Modal,
  message,
  Popconfirm
} from "antd";
import axios from "axios";
import { BASE_URL } from "../services";
import luxcars from "../Images/luxcars.jpg";
import { MoreOutlined } from "@ant-design/icons";
import cogoToast from "cogo-toast";
const { Meta } = Card;

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  margin: 0 4rem 0 4rem;
`;
const CardWrapper = styled.div`
  display: flex;
  flex-direction: column;
  overflow: auto;
  flex-wrap: wrap;
  height: 55vh;
  justify-content: space-around;
`;
const InnerWrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;
const Nodatamsg = styled.div`
  display: flex;
  justify-content: center;
  font-size: 1rem;
  font-weight: bold;
`;

const Title = styled.div`
  font-size: 2rem;
  display: flex;
  justify-content: center;
`;
class ManageAds extends Component {
  state = { visible: false };

  showModal = data => {
    this.setState({
      visible: true,
      clickedVehicle: data
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };
  componentDidMount() {
    const type = localStorage.getItem("type");
    const token = localStorage.getItem("token");
    const email = localStorage.getItem("email");

    this.setState({
      token: token,
      type: type,
      email: email
    });
    this.ViewAllVehicles();
  }

  filterVehicleData = () => {
    let data = this.state.vehicleData;
    let filterData = [];
    console.log(data);
    if (data) {
      data.map(dat => {
        if (dat.isAd) {
          if (!dat.isAdApproved || dat.isAdApproved === null) {
            filterData.push(dat);
          }
        }
      });
    }
    return filterData;
  };

  filterApprovedPosts = () => {
    let data = this.state.vehicleData;
    let filterData = [];

    if (data) {
      data.map(dat => {
        if (dat.isAd && dat.isAdApproved) {
          filterData.push(dat);
        }
      });
    }
    return filterData;
  };
  render() {
    let data = this.filterVehicleData();
    console.log(data);
    let approvedPosts = this.filterApprovedPosts();
    let clickedData = this.state.clickedVehicle;
    console.log(data);
    return (
      <Wrapper>
        <InnerWrapper>
          <Title>Pending Ads for Approval</Title>
          <Divider />
          <CardWrapper>
            {data.length > 0 ? (
              data.map(datum => {
                return (
                  <Card
                    hoverable
                    title={datum.vehicleMake + " " + datum.vehicleModel}
                    style={{
                      width: 240,
                      maxHeight: 345,
                      margin: "0 1rem 1rem 0"
                    }}
                    cover={
                      <img
                        style={{ width: "15rem", height: "10rem" }}
                        alt="example"
                        src={datum.covrimg ? datum.covrimg : luxcars}
                      />
                    }
                    loading={this.state.loading}
                    actions={[
                      <div
                        style={{
                          display: "flex",
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "space-around"
                        }}
                      >
                        <Button
                          onClick={e => this.onApprove(datum.id)}
                          key="approve"
                        >
                          Approve
                        </Button>
                        ,
                        <Button
                          onClick={e => this.onReject(datum.id)}
                          key="edit"
                        >
                          Reject
                        </Button>
                      </div>
                    ]}
                    extra={
                      <Tooltip title="More Details">
                        <Button
                          onClick={e => this.showModal(datum)}
                          style={{ display: "flex", justifyContent: "center" }}
                          shape="circle"
                          icon={<MoreOutlined />}
                        />
                      </Tooltip>
                    }
                  >
                    <Meta
                      // title={datum.vehicleMake + " " + datum.vehicleModel}
                      description={"Price: LKR " + datum.vehiclePrice}
                    />
                  </Card>
                );
              })
            ) : (
              <Nodatamsg>No Pending Approvals</Nodatamsg>
            )}
          </CardWrapper>
        </InnerWrapper>
        <Divider type="vertical" />
        <InnerWrapper>
          <Title>All Approved Ads</Title>
          <Divider />
          <CardWrapper>
            {approvedPosts &&
              approvedPosts.map(pos => {
                return (
                  <Card
                    hoverable
                    title={pos.vehicleMake + " " + pos.vehicleModel}
                    loading={this.state.loading}
                    style={{
                      width: 240,
                      maxHeight: 350,
                      margin: "0 1rem 1rem 0"
                    }}
                    cover={
                      <img
                        style={{ width: "15rem", height: "10rem" }}
                        alt="example"
                        src={pos.covrimg ? pos.covrimg : luxcars}
                      />
                    }
                    actions={[
                      pos.status === "Available" ? (
                        <Popconfirm
                          title={
                            "Are you sure you want remove this approved ad?"
                          }
                          onConfirm={e => {
                            this.onReject(pos.id);
                          }}
                          okText="Yes"
                          cancelText="No"
                        >
                          <Button>Remove Post</Button>
                        </Popconfirm>
                      ) : (
                        ""
                      )
                    ]}
                    extra={
                      <Tooltip title="More Details">
                        <Button
                          onClick={e => this.showModal(pos)}
                          style={{ display: "flex", justifyContent: "center" }}
                          shape="circle"
                          icon={<MoreOutlined />}
                        />
                      </Tooltip>
                    }
                  >
                    <Meta
                      description={
                        "Price: LKR " +
                        pos.vehiclePrice +
                        " " +
                        "Status: " +
                        pos.status
                      }
                    />
                  </Card>
                );
              })}
          </CardWrapper>
        </InnerWrapper>
        {this.state.visible ? (
          <Modal
            title={
              clickedData
                ? clickedData.vehicleModel + " " + clickedData.vehicleYom
                : "No Data"
            }
            visible={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
          >
            {clickedData ? (
              <div style={{ display: "flex", flex: 1, flexDirection: "row" }}>
                <div
                  style={{ display: "flex", flex: 2, flexDirection: "column" }}
                >
                  <p>{"Type: " + clickedData.vehicleType}</p>
                  <p>{"Exterior Color: " + clickedData.vehicleExteriorColor}</p>
                  <p>{"Interior Color: " + clickedData.vehicleInteriorColor}</p>
                  <p>{"Fuel type: " + clickedData.vehicleFuel}</p>
                  <p>{"Mileage: " + clickedData.vehicleMileage}</p>
                  <p>{"Transmission: " + clickedData.vehicleTransmission}</p>
                  {/* <p>{"User email: " + clickedData.user.email}</p> */}
                  <p>{"Price: " + clickedData.vehiclePrice}</p>
                </div>
                <div
                  style={{ display: "flex", flex: 1, flexDirection: "column" }}
                >
                  <img
                    style={{
                      height: "5rem",
                      width: "6rem",
                      marginBottom: "1rem"
                    }}
                    src={clickedData.covrimg ? clickedData.covrimg : luxcars}
                    alt={luxcars}
                  />
                  <img
                    style={{
                      height: "5rem",
                      width: "6rem",
                      marginBottom: "1rem"
                    }}
                    src={
                      clickedData.imageList[1]
                        ? clickedData.imageList[1]
                        : luxcars
                    }
                    alt={luxcars}
                  />
                  <img
                    style={{
                      height: "5rem",
                      width: "6rem",
                      marginBottom: "1rem"
                    }}
                    src={
                      clickedData.imageList[2]
                        ? clickedData.imageList[2]
                        : luxcars
                    }
                    alt={luxcars}
                  />
                </div>
              </div>
            ) : (
              "No Data"
            )}
          </Modal>
        ) : (
          ""
        )}
      </Wrapper>
    );
  }
  UpdateVehicleStatus = (value, token, type) => {
    const payload = {
      vehicleId: value,
      status: "Available"
    };
    axios
      .put(BASE_URL + "api/vehicle/cancelReservation", payload, {
        headers: {
          Authorization: type + " " + token
        }
      })
      .then(res => {
        if (res.status === 200) {
          cogoToast.success("Reservation Cancelled");
        }
      })
      .catch(err => {
        cogoToast.error("Error");
      });
  };
  onApprove = id => {
    let self = this;
    axios
      .put(BASE_URL + `api/vehicle/approveAd/${id}`, null, {
        headers: {
          Authorization: this.state.type + " " + this.state.token
        }
      })
      .then(res => {
        if (res.status == 200) {
          message.success("Ad Approved");
          this.componentDidMount();
        }
      })
      .catch(err => {
        message.error("Error");
      });
  };

  onReject = id => {
    axios
      .delete(BASE_URL + `api/vehicle/deleteVehicle/${id}`, null, {
        headers: {
          Authorization: this.state.type + " " + this.state.token
        }
      })
      .then(res => {
        if (res.status == 200) {
          cogoToast.success("Removed");
          this.componentDidMount();
        }
      })
      .catch(err => {
        cogoToast.error("Error");
      });
  };

  ViewAllVehicles = () => {
    let self = this;
    axios
      .get(BASE_URL + "api/vehicle/getResVehicle")
      .then(res => {
        if (res.status == 200) {
          self.setState({
            vehicleData: res.data,
            loading: false,
            error: false
          });
        }
      })
      .catch(err => {
        self.setState({
          error: true,
          loading: false
        });
      });
  };
}

export default ManageAds;
