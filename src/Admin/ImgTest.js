import axios from "axios";
import React, { Component } from "react";

class ImgTest extends Component {
  state = {};
  onClickHandler = () => {
    //const data = new FormData();
    //data.append("files", this.state.selectedFile);
    let images = this.state.selectedFile;

    const data = new FormData();
    const img = Object.values(images);
    img.map((val, index) => data.append("files", val));

    axios
      .post("http://localhost:8080/upload-multiple-files", data, {
        // receive two parameter endpoint url ,form data
      })
      .then(res => {
        // then print response status
        console.log(res.statusText);
      })
      .catch(err => {
        console.log(err);
      });
  };

  onChangeHandler = event => {
    console.log(event.target.files[0]);
    this.setState({
      selectedFile: event.target.files,
      loaded: 0
    });
  };

  render() {
    return (
      <div>
        <input
          type="file"
          multiple
          name="file"
          onChange={this.onChangeHandler}
        />
        <button
          type="button"
          class="btn btn-success btn-block"
          onClick={this.onClickHandler}
        >
          Upload
        </button>
      </div>
    );
  }
}

export default ImgTest;
