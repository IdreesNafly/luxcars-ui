import React, { Component } from "react";
import styled from "styled-components";
import { Card, Popconfirm, Badge } from "antd";
import axios from "axios";
import { BASE_URL } from "../services";
import cogoToast from "cogo-toast";
import { DeleteOutlined } from "@ant-design/icons";
import luxcars from "../Images/luxcars.jpg";
import { format, differenceInDays } from "date-fns";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  height: 80vh;
  padding: 0px 7rem 2rem 7rem;
  overflow: auto;
`;
const Rowdata = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;
const RowBody = styled.div`
  display: flex;
  flex-direction: row;
  flex: 1;
  border-radius: 5px;
  background: #0000000f;
  padding: 1rem;
  margin-bottom: 1px;
`;

const RowItem = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  font-size: 1.2rem;
  font-weight: 400;
`;

class ReservedVehicles extends Component {
  state = {
    loading: true
  };

  componentDidMount() {
    const token = localStorage.getItem("token");
    const type = localStorage.getItem("type");
    this.setState({
      token: token,
      type: type
    });
    this.getAllReservedVehicles(type, token);
  }

  //   GetFormattedDate(date) {
  //     var todayTime = new Date(date);
  //     var month = format(todayTime.getMonth() + 1);
  //     var day = format(todayTime.getDate());
  //     var year = format(todayTime.getFullYear());
  //     return month + "/" + day + "/" + year;
  //   }

  render() {
    const VEHICLE_DATA = this.state.allVehicle;
    let isExpired = false;
    return (
      <Wrapper>
        {VEHICLE_DATA &&
          VEHICLE_DATA.map(vehicle => {
            let date;
            let today;
            let day;
            if (vehicle.reservedDate) {
              date = format(new Date(vehicle.reservedDate), "MM/dd/yyyy");
              day = new Date(vehicle.reservedDate);
              today = new Date();

              let diff = differenceInDays(day, today);
              if (diff > 3) {
                isExpired = true;
              }
            }
            return (
              <Card
                style={{ display: "flex", marginBottom: "1rem" }}
                hoverable
                bodyStyle={{ display: "flex", flex: 1 }}
                actions={[
                  <Popconfirm
                    placement="topRight"
                    title={"Are you sure you want to cancel users reservation?"}
                    onConfirm={e => {
                      this.UpdateVehicleStatus(
                        vehicle.id,
                        this.state.token,
                        this.state.type
                      );
                    }}
                    okText="Yes"
                    cancelText="No"
                  >
                    <DeleteOutlined
                      key="setting"
                      // onClick={e => this.deleteVehicle(datum.id)}
                    />
                  </Popconfirm>
                ]}
              >
                <div style={{ display: "flex", flex: 0, marginRight: "4rem" }}>
                  <img
                    style={{ height: "8rem", width: "8rem" }}
                    src={vehicle.covrimg ? vehicle.covrimg : luxcars}
                    alt={luxcars}
                  />
                </div>
                <Rowdata>
                  <RowBody style={{ background: "white" }}>
                    <RowItem>
                      {"Vehicle: " +
                        vehicle.vehicleMake +
                        " " +
                        vehicle.vehicleModel}
                    </RowItem>
                    <RowItem>
                      {"Year of Manufacture: " + vehicle.vehicleYom}
                    </RowItem>
                    <RowItem>{"Vehicle Type: " + vehicle.vehicleType}</RowItem>
                    <RowItem>
                      {"Vehicle Price: " + vehicle.vehiclePrice}
                    </RowItem>
                  </RowBody>
                  <RowBody>
                    <RowItem>{"Reserved By: " + vehicle.reservedBy}</RowItem>
                    <RowItem style={{ justifyContent: "center" }}>
                      {date ? "Reserved Date: " + date : "N/A"}
                    </RowItem>
                    <RowItem style={{ justifyContent: "flex-end" }}>
                      <Badge
                        status={isExpired ? "warning" : "processing"}
                        text={isExpired ? "EXPIRED" : "YET TO CONFIRM"}
                      />
                    </RowItem>
                  </RowBody>
                </Rowdata>
              </Card>
            );
          })}
      </Wrapper>
    );
  }

  getAllReservedVehicles(type, token) {
    axios
      .get(BASE_URL + `api/vehicle/getAllReservedVehicles/`, {
        headers: {
          Authorization: type + " " + token
        }
      })

      .then(res => {
        if (res.status === 200) {
          this.setState({
            allVehicle: res.data,
            loading: false
          });
        }
      })
      .catch(err => {});
  }

  UpdateVehicleStatus = (value, token, type) => {
    const payload = {
      vehicleId: value,
      status: "Available"
    };
    axios
      .put(BASE_URL + "api/vehicle/cancelReservation", payload, {
        headers: {
          Authorization: type + " " + token
        }
      })
      .then(res => {
        if (res.status === 200) {
          cogoToast.success("Reservation Cancelled");
          this.componentDidMount();
        }
      })
      .catch(err => {
        cogoToast.error("Error");
      });
  };
}

export default ReservedVehicles;
