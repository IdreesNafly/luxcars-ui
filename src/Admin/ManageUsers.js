import React, { Component } from "react";
import styled from "styled-components";
import axios from "axios";
import { BASE_URL } from "../services";
import {
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBInput,
  MDBBtn,
  MDBTable,
  MDBTableBody,
  MDBTableHead
} from "mdbreact";
import { message, Popconfirm } from "antd";
import "../Stylesheets/AdminStyles.css";
import cogoToast from "cogo-toast";
const Wrapper = styled.div`
  display: flex;
  flex: 1;
  margin: 0 4rem 0 4rem;
`;
const InnerWrapper = styled.div`
  display: flex;
  flex: 1;
`;

const TableWrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

class ManageUsers extends Component {
  state = {
    loading: true,
    error: false
  };
  componentDidMount() {
    const token = localStorage.getItem("token");
    const type = localStorage.getItem("type");

    this.setState({
      token: token,
      type: type
    });
    this.getAllUsers(type, token);
  }
  handleChange = val => {
    console.log(val);
    this.setState({
      selectedRow: val
    });
  };
  handleOnClick = val => {
    let id = this.state.selectedRow;
    console.log(id, "id");
    this.deleteUser(id);
  };

  getTableRows = () => {
    let data = this.state.userData;

    let rows = [];
    if (data) {
      data.map(datum => {
        let row = {
          check: (
            <MDBInput
              style={{ fontSize: "5px", marginLeft: "5rem" }}
              label={""}
              type="checkbox"
              id="checkbox6"
              value={datum.email}
              onChange={e => this.handleChange(datum.email)}
            />
          ),
          email: datum.email,
          fullName: datum.fullName,
          contactNo: datum.contactNo
        };
        rows.push(row);
      });
    }
    console.log(rows);
    return rows;
  };
  render() {
    return (
      <Wrapper>
        <InnerWrapper></InnerWrapper>
        <div>
          <MDBCard narrow>
            <MDBCardHeader className="view view-cascade gradient-card-header blue-gradient d-flex justify-content-between align-items-center py-2 mx-4 mb-3">
              <div>
                <MDBBtn
                  outline
                  rounded
                  size="sm"
                  color="white"
                  className="px-2"
                >
                  <i className="fa fa-th-large mt-0"></i>
                </MDBBtn>
                <MDBBtn
                  outline
                  rounded
                  size="sm"
                  color="white"
                  className="px-2"
                >
                  <i className="fa fa-columns mt-0"></i>
                </MDBBtn>
              </div>
              <a href="#" className="white-text mx-3">
                All Registered Users
              </a>
              <div>
                <MDBBtn
                  outline
                  rounded
                  size="sm"
                  color="white"
                  className="px-2"
                >
                  <i className="fas fa-pencil-alt mt-0"></i>
                </MDBBtn>
                <Popconfirm
                  title={"Are you sure you want to Delete  this user?"}
                  onConfirm={e => {
                    this.handleOnClick();
                  }}
                  okText="Yes"
                  cancelText="No"
                >
                  <MDBBtn
                    outline
                    rounded
                    size="sm"
                    color="white"
                    className="px-2"
                    // onClick={e => {
                    //   this.handleOnClick();
                    // }}
                  >
                    <i className="fas fa-times mt-0"></i>
                  </MDBBtn>
                </Popconfirm>
                <MDBBtn
                  outline
                  rounded
                  size="sm"
                  color="white"
                  className="px-2"
                >
                  <i className="fa fa-info-circle mt-0"></i>
                </MDBBtn>
              </div>
            </MDBCardHeader>
            <MDBCardBody cascade>
              <MDBTable btn fixed>
                <MDBTableHead columns={columns} />
                <MDBTableBody rows={this.getTableRows()} />
              </MDBTable>
            </MDBCardBody>
          </MDBCard>
        </div>
      </Wrapper>
    );
  }
  getAllUsers(type, token) {
    let self = this;
    axios
      .get(BASE_URL + "api/users/getAllUser", {
        headers: {
          Authorization: type + " " + token
        }
      })
      .then(function(res) {
        console.log(res.data);
        self.setState({
          userData: res.data,
          loading: false
        });
      })
      .catch(err => {
        self.setState({
          loading: false,
          error: true
        });
      });
  }

  deleteUser(id) {
    let self = this;
    axios
      .delete(BASE_URL + `api/users/deleteUser/${id}`, {
        headers: {
          Authorization: this.state.type + " " + this.state.token
        }
      })
      .then(function(res) {
        if (res.status === 200) {
          //alert("User Deleted");
          cogoToast.success("User Deleted");
          self.componentDidMount();
        }
      })
      .catch(err => {
        cogoToast.error("Error");
      });
  }
}
const columns = [
  {
    label: "",
    field: "check",
    sort: "asc"
  },
  {
    label: "Email",
    field: "email",
    sort: "asc"
  },
  {
    label: "Full Name",
    field: "fullName",
    sort: "asc"
  },
  {
    label: "Contact Number",
    field: "contactNo",
    sort: "asc"
  }
];

export default ManageUsers;
