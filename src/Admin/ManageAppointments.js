import React, { Component } from "react";
import { Table, Button, Collapse, Divider, message } from "antd";
import axios from "axios";
import styled from "styled-components";
import luxcars from "../Images/luxcars.jpg";
const BASE_URL = "http://localhost:8080/";
const { Panel } = Collapse;
const Wrapper = styled.div`
  display: flex;
  flex: 1;
  margin: 0 4rem 0 4rem;
`;
const InnerWrapper = styled.div`
  display: flex;
  flex: 1;
`;

const TableWrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

class ManageAppointments extends Component {
  state = {};

  getColumn() {
    const columns = [
      {
        title: "Appointment Id",
        dataIndex: "id",
        key: "id",
        render: text => <a>{text}</a>
      },
      {
        title: "Message",
        dataIndex: "message",
        key: "message"
      },
      {
        title: "Appointment Date",
        dataIndex: "appointmentDateTime",
        key: "appointmentDateTime"
      },
      {
        title: "Vehicle ID",
        dataIndex: "vehicleid",
        key: "vehicleid"
      },
      {
        title: "User Email",
        dataIndex: "useremail",
        key: "useremail"
      },

      {
        title: "Action",
        key: "action",
        render: (text, record) => (
          <span>
            <Button
              onClick={e => this.updateStatus(text, "Confirmed")}
              style={{ marginRight: 16 }}
            >
              Accept
            </Button>
            <Button onClick={e => this.updateStatus(text, "Closed")}>
              Close
            </Button>
          </span>
        )
      }
    ];
    return columns;
  }
  getColumn2() {
    const columns = [
      {
        title: "Appointment Id",
        dataIndex: "id",
        key: "id",
        render: text => <a>{text}</a>
      },
      {
        title: "Message",
        dataIndex: "message",
        key: "message"
      },
      {
        title: "Appointment Date",
        dataIndex: "appointmentDateTime",
        key: "appointmentDateTime"
      },
      {
        title: "Vehicle ID",
        dataIndex: "vehicleid",
        key: "vehicleid"
      },
      {
        title: "User Email",
        dataIndex: "useremail",
        key: "useremail"
      }
    ];
    return columns;
  }
  updateStatus = (e, status) => {
    let appointmentid = e.id;

    this.RespondToAppointments(appointmentid, status);
  };
  componentDidMount() {
    const token = localStorage.getItem("token");
    const type = localStorage.getItem("type");

    this.setState({
      token: token,
      type: type
    });

    this.ViewAllAppointments(type, token);
  }
  render() {
    let data = this.state.appointmentData;
    let pending, confirmed, closed;
    if (data) {
      console.log(data);
      pending = data.filter(data => data.status === "Pending");
      confirmed = data.filter(data => data.status === "Confirmed");
      closed = data.filter(data => data.status === "Closed");
    }
    return (
      <Wrapper>
        <TableWrapper style={{ flex: 0.5, alignItems: "center" }}>
          <span style={{ fontSize: "2rem", margin: "1rem" }}>
            Manage All your Appointments
          </span>
          <img src={luxcars} style={{ width: "20rem", height: "20rem" }} />
        </TableWrapper>
        <Divider type="vertical" style={{ height: "100%", width: "10px" }} />
        <TableWrapper>
          <Collapse accordion>
            <Panel header="Confirmed Appointments" key="1">
              <div>
                <Table columns={this.getColumn2()} dataSource={confirmed} />
              </div>
            </Panel>
            <Panel header="Pending Approval" key="2">
              <div>
                <Table columns={this.getColumn()} dataSource={pending} />
              </div>
            </Panel>
            <Panel header="Closed Appointments" key="3">
              <div>
                <Table columns={this.getColumn2()} dataSource={closed} />
              </div>
            </Panel>
          </Collapse>
        </TableWrapper>
      </Wrapper>
    );
  }

  ViewAllAppointments = (type, token) => {
    let self = this;
    axios
      .get(BASE_URL + "api/appointments/getAllAppointment", {
        headers: {
          Authorization: type + " " + token
        }
      })
      .then(function(res) {
        if (res.status === 200) {
          self.setState({
            appointmentData: res.data
          });
        }
      })
      .catch(err => {});
  };
  RespondToAppointments = (id, status) => {
    let self = this;

    const payload = {
      id: id,
      status: status
    };
    axios
      .put(BASE_URL + "api/appointments/respondToAppointment", payload, {
        headers: {
          Authorization: this.state.type + " " + this.state.token
        }
      })
      .then(function(res) {
        if (res.status === 200) {
          message.Success("Responded to Appointment");
          this.componentDidMount();
        }
      })
      .catch(err => {
        message.error("Error");
      });
  };
}

export default ManageAppointments;
