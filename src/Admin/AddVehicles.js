import React, { Component } from "react";
import {
  Select,
  InputNumber,
  Switch,
  Input,
  Radio,
  Slider,
  Upload,
  Rate,
  Checkbox,
  Row,
  Col,
  Menu,
  Form,
  Dropdown,
  Button,
  message,
  Tooltip,
  Card
} from "antd";
import { DownOutlined, UserOutlined } from "@ant-design/icons";
import "../Stylesheets/AdminStyles.css";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import ReactFileReader from "react-file-reader";
import { objectToFormData } from "object-to-formdata";

import { BASE_URL } from "../services";
const { Option } = Select;

class AddVehicles extends Component {
  state = {
    images: [],
    imagesName: []
  };

  componentDidMount() {
    const type = localStorage.getItem("type");
    const token = localStorage.getItem("token");

    this.setState({
      token: token,
      type: type
    });
  }
  onFinish = values => {
    let images = this.state.selectedFile;
    //const form = objectToFormData(values);
    console.log(values);
    const data = new FormData();
    const img = Object.values(images);
    img.map((val, index) => data.append("files", val));
    data.append("vehicle", JSON.stringify(values));
    //data.append("vehicle", JSON.stringify(values));
    // data.append("files", images);
    // data.append("vehicle", values);
    // values.images = images;
    this.AddVehicle(data);
  };

  onChangeHandler = event => {
    console.log(event.target.files);
    this.setState({
      selectedFile: event.target.files,
      loaded: 0
    });
  };

  AddVehicle = val => {
    axios
      .post(BASE_URL + "api/vehicle/addVehicle", val, {
        headers: {
          Authorization: this.state.type + " " + this.state.token
        }
      })
      .then(function(res) {
        let data = res.data;
        if (res.status === 200) {
          message.success("Vehicle Added Successfully");
        }
      })
      .catch(err => {
        message.error("Error");
      });
  };
  render() {
    return (
      <div
        style={{
          padding: "4rem",
          display: "flex",
          flex: 1,
          marginTop: "-5rem",
          height: "135vh"
        }}
      >
        <div
          style={{
            display: "flex",
            flex: 2
            //background: "#000"
          }}
        >
          <img
            style={{ width: "100%" }}
            src="https://www.goldeagle.com/wp-content/uploads/2018/11/478634927.jpg"
            alt=""
          />
        </div>
        <div className="innerWrapper">
          <Card hoverable>
            <Form name="validate_other" onFinish={this.onFinish}>
              <Form.Item
                name="vehicleMake"
                label="Vehicle Make"
                hasFeedback
                rules={[
                  { required: true, message: "Please select Vehicle Make" }
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="vehicleModel"
                label="Vehicle Model"
                hasFeedback
                rules={[
                  { required: true, message: "Please select Vehicle Model!" }
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="vehicleType"
                label="Vehicle Type"
                hasFeedback
                rules={[
                  { required: true, message: "Please select Vehicle Type!" }
                ]}
              >
                <Select placeholder="Please select Vehicle Make">
                  <Option value="SUV">SUV</Option>
                  <Option value="SEDAN">Sedan</Option>
                  <Option value="VAN">VAN</Option>
                  <Option value="COUPE">Coupe</Option>
                </Select>
              </Form.Item>

              <Form.Item
                name="vehicleExteriorColor"
                label="Vehicle Exterior Color"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: "Please select Vehicle Exterior Color!"
                  }
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="vehicleInteriorColor"
                label="Vehicle Interior Color"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: "Please select Vehicle Interior Color!"
                  }
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="vehicleYom"
                label="Vehicle YOM"
                hasFeedback
                rules={[
                  { required: true, message: "Please select vehicle Yom!" }
                ]}
              >
                <InputNumber min={2010} max={2020} />
              </Form.Item>
              <Form.Item
                name="vehicleDescription"
                label="Vehicle Description;"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: "Please enter Vehicle Description!"
                  }
                ]}
              >
                <Input placeholder="Please enter Vehicle Description" />
              </Form.Item>

              <Form.Item
                name="vehicleFuel"
                label="Vehicle Fuel"
                hasFeedback
                rules={[
                  { required: true, message: "Please select Vehicle Fuel!" }
                ]}
              >
                <Radio.Group>
                  <Radio.Button value="Hybrid">Hybrid</Radio.Button>
                  <Radio.Button value="Petrol">Petrol</Radio.Button>
                  <Radio.Button value="Diesel">Diesel</Radio.Button>
                </Radio.Group>
              </Form.Item>

              <Form.Item
                name="vehicleTransmission"
                label="Vehicle Transmission"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: "Please select vehicleTransmission!"
                  }
                ]}
              >
                <Radio.Group>
                  <Radio.Button value="Automatic">Automatic</Radio.Button>
                  <Radio.Button value="Manual">Manual</Radio.Button>
                </Radio.Group>
              </Form.Item>

              <Form.Item
                name="vehicleMileage"
                label="Vehicle Mileage"
                hasFeedback
                rules={[
                  { required: true, message: "Please enter Vehicle Mileage!" }
                ]}
              >
                <InputNumber min={1} max={100000} />
              </Form.Item>

              <Form.Item
                name="vehiclePrice"
                label="Vehicle Price"
                hasFeedback
                rules={[
                  { required: true, message: "Please enter Vehicle Price!" }
                ]}
              >
                <InputNumber defaultValue={1000} />
              </Form.Item>
              <Form.Item label="Upload Vehicle Images">
                <input
                  type="file"
                  name="file"
                  multiple
                  onChange={this.onChangeHandler}
                />
              </Form.Item>

              <Form.Item>
                <Button type="primary" htmlType="submit">
                  Add Vehicle
                </Button>
              </Form.Item>
            </Form>
          </Card>
        </div>
      </div>
    );
  }
}

export default AddVehicles;
