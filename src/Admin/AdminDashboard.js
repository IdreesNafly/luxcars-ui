import React, { Component } from "react";
import "../Stylesheets/AdminStyles.css";
import { Card } from "antd";
import { Link, withRouter } from "react-router-dom";

const gridStyle = {
  width: "48%",
  height: "15vh",
  textAlign: "center",
  margin: "0.5rem",
  fontSize: "2rem",
  color: "white",
  fontVariantCaps: "all-petite-caps"
};

const cardStyle = {
  display: "flex",
  justifyContent: "center",
  marginTop: "-6rem",
  background: "#053361",
  width: "60%"
};

const wrapper = {
  display: "flex",
  flex: 1,
  justifyContent: "center",
  alignItems: "center",
  flexDirection: "column"
};
const card = {
  display: "flex",
  //height: "15vh",
  width: "72.5%"
  //background: "black"
};

class AdminDashboard extends Component {
  state = {};
  render() {
    return (
      <div style={wrapper}>
        <div
          style={{
            fontSize: "2rem",
            color: "black",
            position: "relative",
            fontStyle: "italic",
            fontVariantCaps: "unicase",
            marginTop: "-2rem"
          }}
        >
          Admin Panel
        </div>
        <img
          style={{ width: "95%" }}
          src="https://www.carmax.com/home/images/home/call-out/red-car.png"
          alt=""
        />
        <Card style={cardStyle}>
          <Link to="/adminmanageusers">
            <Card.Grid style={gridStyle}>Manage Users</Card.Grid>
          </Link>
          <Link to="/adminaddvehicles">
            <Card.Grid style={gridStyle}>Add Vehicles</Card.Grid>
          </Link>
          <Link to="/adminmanageappointments">
            <Card.Grid style={gridStyle}>Manage Appointments</Card.Grid>
          </Link>
          <Link to="/adminmanageads">
            <Card.Grid style={gridStyle}>Manage Ads</Card.Grid>
          </Link>
          <Link to="/adminmanagevehicles">
            <Card.Grid style={gridStyle}>Manage Vehicles</Card.Grid>
          </Link>
          <Link to="/adminreservedvehicles">
            <Card.Grid style={gridStyle}>Reserved Vehicles</Card.Grid>
          </Link>
        </Card>
      </div>
    );
  }
}

export default withRouter(AdminDashboard);
