export const VEHICLE_MAKE = [
  "BMW",
  "Toyota",
  "Honda",
  "Audi",
  "Ford",
  "GMC",
  "Mitsubishi",
  "Kia",
  "Nissan",
  "Chevrolet",
  "Aston Martin",
  "Mercedes-Benz"
];

export const VEHICLE_MODEL=[
 "A1",
" S1",
" A2",
" A3",
" S3",
" A4",
" S4",
" RS4",
" A4 allroad quattro",
" A5",
" S5",
" RS5",
" A6",
" S6",
" RS 6",
" A6 allroad quattro",
" A7",
" RS7",
" S7",
" A8",
" S8",
" Q2",
" Q3",
" Q5",
" Q7",
" Q8",
" R8",
" TT",
" V8",
" e-tron",

]

export const CONDITION=[

]

export const YEAR=[

]
